program starydom;
uses c64, crt, graph, deflate;

const 
{$i const.inc}
{$r resources.rc}
{$i io.inc}
{$i lang.inc}
{$i charset.pas}
{$i debug.pas}
{$i sysutil.pas}
{$i music.pas}

var 
{    msx						      : TRMT;}
    inventory, currentPassword, weaponName		      : TString;
    aStr, bStr						      : TString; // reusable temporary strings
    x, y						      : byte; // player Position
    i							      : byte; // common iterator
    monster						      : byte;
    consecutiveWaits, seenPassword			      : byte;
    currentRoomTile, {keycode,} roomDifficulty, maxDifficulty : byte;
    keycode						      : char;
    q, r						      : byte;  // randoms
    weapon, wounds					      : shortInt;
    moves, gold						      : smallInt;
    score						      : cardinal;
    pconsol						      : byte;
    strength, energy, monsterStrength, monsterSize	      : real;
    gameEnded						      : boolean;
    musicOn						      : boolean = false;
    lootHistory						      : array[0..ITEMS_COUNT-2] of byte;


// ************************************* helpers

function StrCmp(a, b: TString): boolean;
var i, len_a, len_b : byte;
begin
    result := true;
    len_a := length(a) - 1;
    len_b := length(b) - 1;
    if ( len_a <> len_b ) then exit(false);
//    for i:=0 to length(a)-1 do
    for i:=0 to len_a do
	if a[i]<>b[i] then exit(false);
end;

function FormatFloat(num: real):TString;
var m: cardinal;
    ms: TString;
begin
    Str(Trunc(num), result);
    m := Trunc(Frac(num) * 1000.0);
    if m > 0 then begin
        Str(m, ms);
        while (length(ms) < 3) do ms := concat('0', ms);
        result := concat(result, '.');
        result := concat(result, ms);
        while (result[byte(result[0])]='0') do dec(result[0]);
    end;
end;

{ https://retrocomputing.stackexchange.com/questions/21738/how-to-wait-for-vblank-on-commodore-64 }
procedure WaitFrame();assembler;
asm
WaitFrame      bit $d011
               bpl WaitFrame
               lda $d012
WaitFrameLoop
               cmp $d012
               bmi WaitFrameLoop
end;


// ************************************* initializers

procedure VarInit;
begin
    x := 6; 
    y := 1; 
    roomDifficulty := 1;
    maxDifficulty := 15 * 8;
    fillchar( @inventory[1], 4, TILE_EMPTY_SLOT);
    inventory[0] := char(4);
    currentPassword := passwords[Random(9)];
    consecutiveWaits := 0;
    weapon := 1; 
    gold := 0;
    wounds := 0;
    energy := 3; 
    seenPassword := 0;
    moves := 0;
    currentRoomTile := TILE_ROOM;
    weaponName := weapons[weapon - 1];
    fillByte( @lootHistory, ITEMS_COUNT - 1, 0);
end;

// ************************************* GUI

procedure PromptAny;
begin
    Position(19 - (Length(s_PRESS_ANY) shr 1), 23);
    Write(s_PRESS_ANY);
    Pause(20);
    ReadKey;

    {fillbyte ( pointer(savmsc),24*40,0);}
    FillByte ( pointer(SCREEN_BASE), 25*40, 0 );
    Position(0,0);
end;


(*
procedure SetBorderColor(color : byte);
begin
    Bordercolor := color;
    //poke ( 53280,0 );
end;
*)

procedure ClearScreenColor(color : byte);
begin
    //fillByte( pointer ($5000), 1000, color); // tw - update the address(!)
    fillByte( ScreenRAM, 1000, color); 
end;
			 
procedure TitleScreen;
const
    ncolors : byte = 5;
    colors  : array[ 0 .. ncolors - 1] of byte = (
       { 1,    11,  12,  16,  27,  28,  31,
        176, 177, 187, 188, 190, 192, 202,
        203, 206, 239, 240, 250, 251, 255}
        $00, $B0, $C0, $F0, $10 );
var
    i	: byte;
    key	: char;
begin
{    InitGraph(VGAHi);}
    BorderColor := BLACK;
    SetColor(1);
    ClearScreenColor(BLACK);
    //readkey;
    Pause;
    //Pause(200);
{    nmien := $40;
    color2 := 0;
    color1 := 0;
    Move ( pointer(TITLE_BASE), pointer(savmsc + (40 * 8)), 5840);}
    Move ( pointer(TITLE_BASE), pointer( SCREEN_BASE + ( 40 * 8 ) ),
	   //6080 ); // c64 title screen is bigger - has 148, not 146 lines(!)
           //        // (as it must be divisible by 8!)
	   6400 );  // enlarged 146 -> 160 lines
    unDEF ( pointer(TITLE_BASE), pointer( SCREEN_BASE + ( 40 * 8 ) ) );
    //Pause(200);
    Pause(110);
{    Poke ( $6100, $FF ); 
    SetBkColor(1);
    SetColor(0);
    PutPixel ( 100,100);

    SetColor (1);
    MoveTo (10, 10);
    LineTo (50, 50);}
    {SwitchDisplayBuffer(0,1);}
    {Move ( pointer(TITLE_BASE), pointer(SCREEN_BASE), 5840 );}

    // Fade-in
{    for i:=0 to 12 do begin
        color1 := i;
        Pause(2);
    end;}
    //for i:=0 to ncolors-1 do begin
    for i:=0 to 2 do begin
	WaitFrame();
	ClearScreenColor ( colors[i] );
        Pause(5);
    end;

    Pause(200);

    // Lightning
    repeat
	for i:=0 to 1 do
	begin
	    WaitFrame();
	    ClearScreenColor ( $10 );
	    BorderColor := BLACK;
	    Pause(5);
	    WaitFrame();
	    ClearScreenColor ( $01 );
	    BorderColor := WHITE;
	    Pause(5);
	end;
	WaitFrame();
	//SetBorderColor(0);
	BorderColor := BLACK;
	ClearScreenColor ( $C0 );
	//key := readkey;
	key := GetKey();
    until key = #32;
    (*
    repeat
	for i:=0 to 1 do
	begin
	    for i:=0 to 4 do begin
		WaitFrame();
		ClearScreenColor ( colors[i] );
		//Pause(1);
	    end;
	    WaitFrame();
	    ClearScreenColor ( $10 );
	    BorderColor := BLACK;
	    for i:=4 downto 0 do begin
		WaitFrame();
		ClearScreenColor ( colors[i] );
		//Pause(1);
	    end;
	    WaitFrame();
	    ClearScreenColor ( $01 );
	    BorderColor := WHITE;
	end;
	WaitFrame();
	ClearScreenColor ( $C0 );
	//SetBorderColor(0);
	BorderColor := BLACK;
	key := readkey;
    until key = #32;
   *)

    
    // Fade-out
    {for i:=ncolors-1 to 0 do begin}
    for i:=2 downto 0 do begin
	WaitFrame();
	ClearScreenColor ( colors[i] );
        Pause(7);
    end;
    WaitFrame();
    {   for i:=12 to 0 do begin
        color1 := i;
        Pause;
    end;}
end;

procedure ShowManual;
begin
    {InitGraph(0);}
    { https://www.c64-wiki.com/wiki/Character_set }
    //SetCharset();
    //pause;
    {nmien := $40;
    color2 := 0;}
    TextBackGround(0);
    TextColor(LIGHT_GREY);
    CursorOff;
    ClrScr;
    {chbas := Hi(CHARSET_BASE);}
    Writeln;
    //Poke ( $D018, $17 ); //(53272, 23);  { set charset 2 (small and large letters) }
    Poke ( $D020, $0 );   // black background (and border)
    Poke ( $D021, $0 );
    Writeln(s_WANT_MANUAL);
    keycode := GetKey;
    //write ('keycode: ');
    //writeln (keycode);
    //readkey;
    //readkey;
    if keycode <> k_YES then exit;

    ClrScr;
    TextColor(GREY);
    ManualPage1;
    TextColor(LIGHT_GREY);
    PromptAny;

    ClrScr;
    TextColor(GREY);
    ManualPage2;
    TextColor(LIGHT_GREY);
    PromptAny;

    ClrScr;
    TextColor(GREY);
    ManualPage3;
    //Position(0,24);
    TextColor(LIGHT_GREY);
    PromptAny;

    {ClrScr;
    ShowCharacters;
    PromptAny;    }
end;


function GetRandomEntranceV : byte;
var r : byte;
begin
    result := TILE_ENTRANCE_V;
    r := Random(5);
    if r = 0 then result := TILE_DOOR_V;
    if r = 1 then result := TILE_WALL_V;
end;

function GetRandomEntranceH : byte;
var r : byte;
begin
    result := TILE_ENTRANCE_H;
    r := Random(5);
    if r = 0 then result := TILE_DOOR_H;
    if r = 1 then result := TILE_WALL_H;
end;


procedure PaintBoard;
var row, room : byte;
begin
    
    //initGraph(0);
    CursorOff;
    {color2 := 0;
    color1 := 0;
    savmsc := SCREEN_BASE;}
    
    VarInit;
    ClrScr;
    {
    fillbyte ( pointer(TXT_SCREEN_BASE),                 40 * 17, 128 );
    fillbyte ( pointer(TXT_SCREEN_BASE + 40 * 17 ),      5,       $ff );
    fillbyte ( pointer(TXT_SCREEN_BASE + 40 * 17 + 36 ), 4,       $ff );}
    
    Position(5, 0); // top row of board
    //Writeln('abc');
    //Write(#17);
    PrintRaw ( 112 );
    for room:=1 to 14 do
        //Write( char(TILE_BORDER_H), #23);
	PrintRaw ( TILE_BORDER_H, 114 );
    //Write( char(TILE_BORDER_H), #5);
    PrintRaw ( TILE_BORDER_H, 110 );

    for row:=0 to 7 do begin
    
        // rooms, vertical walls
        Position(5, row * 2 + 1);
	PrintRaw ( TILE_BORDER_V );
        for room:=1 to 14 do
            PrintRaw ( TILE_ROOM, GetRandomEntranceV );
        PrintRaw ( TILE_ROOM, TILE_BORDER_V );

        // inner horizontal walls, doors
        Position(5, row * 2 + 2);
        PrintRaw ( 107 );   // left border wall
        for room:=1 to 14 do 
            PrintRaw ( GetRandomEntranceH, 91 ); // door, inner wall (corner / 'cross')
        PrintRaw ( GetRandomEntranceH, 115 ); // right border wall
    end;

    // bottom row of board
    Position(5, 16);
    PrintRaw ( 109 );
    for room:=1 to 14 do
	PrintRaw ( TILE_BORDER_H, 113 );
    PrintRaw ( TILE_BORDER_H, 125 );

    // entrance, exit, player
    Position(7, 1);   PrintRaw ( TILE_ENTRANCE_V );
    Position(6, 2);   PrintRaw ( TILE_ENTRANCE_H );
    Position(x, y);   PrintRaw ( TILE_PLAYER );
    Position(34, 15); PrintRaw ( TILE_EXIT, TILE_EXIT2 );
   
    Pause;
    //SetCharset();
    {chbas := Hi(CHARSET_BASE);
    SDLSTL := DISPLAY_LIST_BASE;
    nmien := $c0;} // set $80 for dli only (without vbl)
    //color2 := $10;
    //color1 := $10;
    for i:=$10 to $1a do begin
        pause(2);
        //color2 := i;
    end;
end;

function GetItemName(item : char) : TString;
var i	 : byte;
begin
    for i:=0 to ITEMS_COUNT - 2 do
	if item = itemSymbols[i]
        then begin
	    Result := items[i];
	    exit;
	end;
    Result := '-';
end;

procedure ShowStats;
var z: real;
begin
    TextColor(COLOR_STATUS);
    if energy < 0 then energy := 0;
    Position(0, 18);  Write(s_ENERGY, '   ', formatFloat(energy), '    ');
    z := energy - wounds;
    if z < 0 then z := 0.1;
    Position(0, 19);  Write(s_TREASURE,' $', gold, '  ');
    Position(0, 20);  Write(s_WEAPON, '   ', weaponName,'     ');
    strength := z * (1 + weapon * 0.25);
    Position(0, 21);  Write(s_ATTACK, '   ', formatFloat(strength), '          ');

    Position(18, 18); Write(s_WOUNDS, ' ', wounds, ' ');
    Position(18, 20); Write(s_MOVES, ' ', moves,' ');

    // show items
    //Position(24, 19);
    //Write(s_ITEMS,'  ',inventory);
    //Write(s_ITEMS);
    {
    Position(27, 18); Write('I');
    Position(27, 19); Write('T');
    Position(27, 20); Write('E');
    Position(27, 21); Write('M');
    Position(27, 22); Write('S');}
    for i:=1 to 4 do
    begin
	Position( 29, 17 + i );
	Write ('          ');
	Position( 29, 17 + i );
	//Write( inventory[i] );
	Write ( GetItemName( inventory[i] ) );
    end
end;

procedure KeyAndShowStat;
begin
    Readkey;
    ShowStats;
end;


// ************************************* inventory operations


function HasItem(c: char):boolean;
var i: byte;
begin
    result := false;
    for i := 1 to 4 do 
        if inventory[i] = c then exit(true);
end;

procedure DelItem(c: char);
var i: byte;
begin
    for i := 1 to 4 do 
        if inventory[i] = c then begin
            inventory[i] := char(TILE_EMPTY_SLOT);
            exit;
        end;
end;

procedure AddItem(c: char);
var i: byte;
begin
    for i := 1 to 4 do 
        if inventory[i] = char(TILE_EMPTY_SLOT) then begin
            inventory[i] := c;
            exit;
        end;
end;

function HasAnythingToUse:boolean;
begin
    result := HasItem(itemSymbols[6]) or
              HasItem(itemSymbols[7]);
end;

// ********************************************** main turn logic

procedure MakeMove;
var door, room				    : byte;
    dx, dy				    : shortInt;
    isIn, stepFinished, waited, skipMonster : boolean;
    itemLost				    : char;
    lootValue				    : smallInt;


procedure PayRansom;
begin
    gold := round(gold - round(monsterSize * Random));  // pay ransom
    if gold < 0 then gold := 0;
    stepFinished := true;
    ShowStats;
end;    

procedure FoundWeapon;
begin
    r := Random(10) + 1;    // 1-10
    StatusLine(s_FOUND);
    Write( GetArticle( weapons[r - 1] ), ' ', weapons[r - 1]);

    //Position(20,24);
    Position(12, 24);
    Write(s_TAKE, s_OR, s_LEAVE,' ?');
    Pause(50);
    keycode := getKey(k_TAKE, k_LEAVE);
    if keycode = k_TAKE then begin
        weaponName := weapons[r - 1];
        weapon := r;
        ShowStats;
    end;
end;

procedure FoundPassword;
begin
    if seenPassword < 3 then begin
        StatusLine(s_FOUND_PASS);
        Write(currentPassword, '.');
	StatusLinePos2;
        Write(s_REMEMBER);
        Pause(200);
        inc(seenPassword);
    end;
end;

procedure DecLootCounters;
var item:byte;
begin
    for item:=0 to ITEMS_COUNT-2 do 
        if lootHistory[item]>0 then Dec(lootHistory[item]);
end;

function GetMonster: byte;
var monsterLevel: shortInt;
begin
    monsterLevel := (roomDifficulty * MONSTERS_COUNT) div maxDifficulty;
    monsterLevel := monsterLevel - 4 + Random(8);
    if monsterLevel < 0 then monsterLevel := 0;
    monsterStrength := Round(Random(byte((monsterLevel * 2) + 1)) + (roomDifficulty + strength / 10)) + 1;
    monsterSize := monsterStrength;
    if monsterLevel >= MONSTERS_COUNT then monsterLevel := MONSTERS_COUNT - 1;
    result := monsterLevel;
end;

procedure FoundItem;
var 
    item : byte;
begin 
    DecLootCounters;
    repeat 
        item := Random(ITEMS_COUNT-1);
    until lootHistory[item] = 0;
    lootHistory[item] := 6;
    StatusLine(s_FOUND);
    Write( GetArticle( items[item] ), ' ', items[item] );

    //Position(20, 24);
    Position(12, 24);
    Write(s_TAKE, s_OR, s_LEAVE,' ?');
    keycode := GetKey(k_TAKE, k_LEAVE);
    if keycode = k_LEAVE then begin
        foundWeapon;
    end else begin
        if (item = 4) or (item = 5) then begin
            case item of
                4: energy := energy + 3;
                5: energy := energy + 1;
            end;
            ShowStats;
	end else begin
            if hasItem( char(TILE_EMPTY_SLOT) ) then begin
		addItem(itemSymbols[item]);
                ShowStats;
            end else begin
                stepFinished := false;
                repeat
		    StatusLine2(s_LEAVE_WHAT);
		    Pause(10);
		    repeat
                        keycode := getKey;
                    until ( ord(keycode) > 65) and ( ord(keycode) < 90);
                    if hasItem(char(keycode)) then begin
                        DelItem(char(keycode));
                        addItem(itemSymbols[item]);
                        ShowStats;
                        stepFinished:=true;
                    end else begin
                        StatusLine2(s_DONT_HAVE);
                        Write(#39, char(keycode), #39'         ');
                        KeyAndShowStat;
			Pause(30);
                    end;
                until stepFinished;
            end;
        end;
    end;
end;
    
procedure GetLoot();
begin
    r := Random(11) + 1; 
    case r of
        1,2,3,4,5,6,7,8: foundItem;
        9: foundWeapon;
        10: FoundPassword;
    end;
end;    

procedure MovePlayer(nx, ny:byte);
begin
    consecutiveWaits := 0;
    Position(x, y);
    PrintRaw ( currentRoomTile );
    x := nx;
    y := ny;
    currentRoomTile := Locate(x, y);
    Position(x, y);
    PrintRaw ( TILE_PLAYER );
    roomDifficulty := ((x - 4) shr 1) * ((y + 1) shr 1);
end;
    
begin 
    ShowStats;
    isIn := false;
    stepFinished := false;
    skipMonster := false;
    waited := false;

    ClearStatusLine;
    Position(10, 23);
    TextColor(COLOR_ACTION);
    Write(s_WAIT, s_OR, s_MOVE, ' ?');
    keycode := getKey(k_REST, k_MOVE);

    if keycode = k_REST then begin      // ************* waiting
        if gold < 5 then begin
            energy := energy + 0.5;
        end else begin
            gold := gold - 5;
            energy := energy + 2;
        end;
        waited := true;
        stepFinished := true;
        ShowStats;
        Inc(consecutiveWaits);
    
    end else begin                  // ************* moving
        Inc(consecutiveWaits);
        Position(7, 23);     
	Write(s_LEFT,', ',s_RIGHT,', ',s_UP,', ',s_DOWN,' ?');
	keycode := getKey(k_LEFT, k_RIGHT, k_UP, k_DOWN);
        ClearStatusLine;
        dx := 0;
        dy := 0;
        case keycode of
            k_LEFT: dx := -1;
            k_RIGHT: dx := 1;
            k_DOWN: dy := 1;
            k_UP: dy := -1;
        end;
        
        door := Locate(x + dx, y + dy);
        //Position(1,1); Write(door);
        
        if (door <> TILE_BORDER_H) and (door <> TILE_BORDER_V) then begin // not a border ?
        
            if (door = TILE_ENTRANCE_H) or (door = TILE_ENTRANCE_V) then begin     // ***********************  check doors
                StatusLine(s_DOOR_OPENED);
                isIn := true;
            end else begin
                if (door = TILE_DOOR_H) or (door = TILE_DOOR_V) then begin
                    StatusLine(s_DOOR_CLOSED);
		    StatusLinePos2;
                    if hasItem(itemSymbols[2]) then begin
                        Write(s_USED, s_KEY, '.');
                        isIn := true;
                    end else begin
                        Write(s_DONT_HAVE, s_BYKEY, '.');
                    end;
                end;
                if (door = TILE_WALL_H) or (door = TILE_WALL_V) then begin
                    StatusLine(s_WALL);
		    StatusLinePos2;
                    if hasItem(itemSymbols[0]) then begin
                        Write(s_USED, s_HAMMER, '.');
                        isIn := true;
                    end else begin
                        Write(s_DONT_HAVE, s_BYHAMMER, '.');
                    end;
                end;
                if isIn then begin
                    energy := energy - 0.5;
                    Position(x + dx, y + dy);
                    if dy=0 then door := TILE_ENTRANCE_V
                    else door := TILE_ENTRANCE_H;
                    //Print(char(door));
		    PrintRaw(door);
                end;
            end;

	    Pause(100);
	    //ReadKey;
            
            if isIn then begin  // *******************************   check room
                room := Locate(x + 2 * dx, y + 2 * dy);
                //room := Locate(34, 15);   // for testing the end of the game
                if room = TILE_ROOM then begin
                    q := Random(2);
                    if not waited and (x > 8) and (y > 3) and (Random(10) >= 4) then begin
                        if q = 0 then room := TILE_DARK
                        else room := TILE_HOLE;
                    end else begin
                        r := Random(6);
                        if r = 0 then begin
                            if q = 0 then room := TILE_DARK
                            else room := TILE_HOLE;
                        end;
                    end;
                end;
                
                if room = TILE_DARK then begin    //  ********************* dark room
                    Position(x + 2 * dx, y + 2 * dy);
                    //Print(char(TILE_DARK));
		    PrintRaw(TILE_DARK);
                    StatusLine(s_ROOM_DARK); 
                    StatusLinePos2;
                    if hasItem(itemSymbols[1]) then begin
                        Write(s_USED, s_LANTERN, '.');
                        isIn := true;
                    end else begin
                        Write(s_DONT_HAVE, s_BYLANTERN, '.');
                        isIn := false;
                    end;
		    Pause(200);
                    //ReadKey;
                    ClearStatusLine;                    
                end;
                if room = TILE_HOLE then begin    //  ********************* no floor
                    Position(x + 2 * dx, y + 2 * dy);
		    //Print(char(TILE_HOLE));
		    PrintRaw(TILE_HOLE);
                    StatusLine(s_ROOM_HOLE);
                    StatusLinePos2;
                    if hasItem(itemSymbols[3]) then begin
                        Write(s_USED, s_PLANK, '.');
                        isIn := true;
                    end else begin
                        Write(s_DONT_HAVE, s_BYPLANK, '.');
                        isIn := false;
                    end;
		    Pause(200);
		    //ReadKey;
                end;
                if room = TILE_EXIT then begin  //  ********************* exit reached
                    Position(x, y);
                    PrintRaw ( TILE_ROOM );
                    Position(34, 15);
                    PrintRaw ( TILE_PLAYER );
                    StatusLine(s_EXIT_PASS);
                    aStr:='';
                    CursorOn;
                    //Position(2, 24);
                    StatusLinePos2;
		    TextColor(WHITE);
                    for i := 1 to 4 do begin
                        keycode := getKey;
                        aStr[i] := char(keycode);
			//Print(char(keycode));
                        PrintRaw(ord(keycode));	  // tw / to check!
			Pause(20);
                    end;
                    CursorOff;
                    aStr[0] := chr(4);
                    if strCmp(aStr, currentPassword) then
		    begin
			StatusLine(s_EXIT_PAY);
                        ReadKey;
                        if gold >= 100 then
			begin
                            gold := gold - 100;
                            score := Trunc(gold * weapon);
                            StatusLine(s_EXIT_LEAVE);
                            Write('$', gold);
                            StatusLine2(s_AND);
			    Write( GetArticle(weaponName), ' ', weaponName,
				  s_EXIT_SCORE, score);
                            Pause(3);
                            KeyAndShowStat;
                            gameEnded := true;
                        end else
			begin
                            StatusLineln(s_EXIT_POOR);
                            Write(s_EXIT_FATAL);
                            Pause(3);
                            Readkey;
                            gameEnded := true;
                        end;
		    end else
		    begin
                        StatusLine(s_EXIT_WRONG_PASS);
                        Write(currentPassword, '.');
                        StatusLine2(s_EXIT_FATAL);
                        Pause(3);
                        Readkey;
                        gameEnded := true;
                    end;
		    Pause(500);
		    ReadKey;
                end;
            end;
            
            if isIn and not gameEnded then begin // ***********   entered new room, update map
                MovePlayer(x + 2 * dx, y + 2 * dy);
                energy := energy - 0.5;
                stepFinished := true;
            end;
            
    
        end else begin  // **********************************   hit the wall
            StatusLineln(s_BUMP);
            Write(s_NO_PASARAN);
            energy := energy - 0.5;
            KeyAndShowStat;
        end;        
    end;
    
    if stepFinished and not gameEnded then begin  // ********************  Random events
    
        r := Random(40)+3;
        if r < consecutiveWaits then begin
            if not ( ( x = 6 ) and ( y = 1 ) ) then
            begin
                StatusLine(s_BACK_TO_START);
                MovePlayer(6, 1);
                //Readkey;
                Pause(200);
            end;
            ShowStats;
        end else begin
            r := Random(15);
            itemLost := char(TILE_EMPTY_SLOT);
            case r of
                0,1,2,3,4,5,6,7: 
                   if hasItem(itemSymbols[r]) then begin 
                        StatusLine(s_ITEM_BROKE[r]);
                        itemLost := itemSymbols[r];
                   end;
                8: if Random(10) >= 5 then begin
                        FoundPassword;
                        skipMonster := true;
                   end;
                9,10: 
                    begin
                        if weapon > 1 then begin 
                            StatusLine(s_BROKE);
                            Write( GetArticle(weaponName), ' ', weaponName, ', ');
                            weapon := weapon - 4;
                            if weapon < 1 then weapon := 1;
                            weaponName := weapons[weapon - 1];
			    StatusLinePos2;
                            Write(s_FOUND, weaponName, '.');
			    Pause(200);
                            //Readkey;
                            ShowStats;
                        end;
                    end;
            end;

            if itemLost <> char(TILE_EMPTY_SLOT) then begin
                //Position(2, 24);
                StatusLinePos2;
                Write(s_DROPPED);
                DelItem(itemLost);
		//Readkey;
		Pause(200);
                ShowStats;
            end;
        end;
    
    end;
    
    if not skipMonster and not gameEnded then begin
        r := Random(4);
        if r>0 then begin  // ********************************** encounter !!!
            monster := GetMonster;
            aStr := monsters[monster];
            bStr := s_YOU_M;
            if needPostfix(monster) then bStr := s_YOU_F;
	    StatusLine(s_YOU_ARE); write(s_ATTACKED);
	    Write( bStr, GetArticle(aStr), ' ', aStr,' ');
            Write(s_MONSTER_STR, formatFloat(monsterStrength));

            stepFinished := false;
            repeat
                
                if ((strength = 0) or (wounds > 4)) and (gold = 0) then begin
                    //Position(2,24);
                    StatusLinePos2;
                    Write(s_TOO_WEAK_POOR);
                    Pause(100);
                    Readkey;
                    StatusLine(s_BACK_TO_START);
                    MovePlayer(6, 1);
                    stepFinished := true;
                    wounds := 0;
                    KeyAndShowStat;
                end else begin 
                    if (strength = 0) or (wounds > 4) then begin // ***********     too weak ?
                        //Position(2,24);
                        StatusLinePos2;
                        Write(s_TOO_WEAK);
                        keycode := k_RANSOM;
			//KeyAndShowStat;
			Pause(200);
			ShowStats;
                    end else if gold = 0 then begin             // ************** no gold ?
                        //Position(2,24);
                        StatusLinePos2;
                        Write(s_TOO_POOR);
                        keycode := k_FIGHT;
			//KeyAndShowStat;
			Pause(200);
			ShowStats;
                    end else begin
                        Position(12,24);
                        Write(s_FIGHT_OR_BRIBE, ' ?');
                        keycode := getKey(k_FIGHT, k_RANSOM);
                    end;
                end;
                
                if keycode = k_FIGHT then begin  // ************** fight chosen
                    // get hurt
                    if (strength < monsterStrength * 1.2) and (Random(10) >= 4) then   
                        wounds := wounds + 1;
                    // hit monster
                    monsterStrength := round(monsterStrength -
					     ((Random * 2) + 1) * strength * 0.57);
                    
                    ShowStats;
                    
                    if (monsterStrength <= 0) or (Random(10) >= 5) then begin // ********* monster killed
                        r := Random(5) + 1;  // loot size
                        lootValue := round(r * (1.25 * (1 + monsterSize / 15) + Random));
                        StatusLine(s_THE); Write(aStr,s_HAS_BEEN);
                        if needPostfix(monster) then Write(s_DEFEATED_F)
                        else                         Write(s_DEFEATED_M);
                        //Position(2,24);
                        StatusLinePos2;
                        Write(s_EARNED, treasures[r - 1], ', VALUE $', lootValue);
                        gold := gold + lootValue;
                        stepFinished := true;
			//KeyAndShowStat;
			Pause(200);
			ShowStats;
                    end else begin
			StatusLine(s_THE);
			Write( aStr, s_HAS_STR, formatFloat(monsterStrength) );
			Pause(200);
                    end;
                end else
		    PayRansom // ************** pay ransom
            until stepFinished;
        end;
        GetLoot;
    end;

    if not gameEnded then begin // *********************************** use items
        stepFinished := false;
        if HasAnythingToUse then
            repeat 
                StatusLine(s_WANNA_USE);
		Pause(10);
                keycode := getKey(k_YES, k_NO);
                if keycode = k_YES then begin
                    StatusLine2(s_WHICH);
		    Pause(10);
                    repeat 
                        keycode := getKey;
                    until (ord(keycode) > 65) and
		          (ord(keycode) < 90);
                    if not hasItem(char(keycode)) then begin
                        StatusLine2(s_DONT_HAVE);
			//Write(#39, chr(keycode), #39);
			Write(#39, keycode, #39);
			Pause(50);
                        //ReadKey;
                    end else
                        if (keycode = k_BANDAGE) or
			   (keycode = k_MEDKIT)
			then begin
                            DelItem(char(keycode));    
                            case keycode of
                                k_BANDAGE: wounds := wounds - 1;
                                k_MEDKIT:  wounds := wounds - 3;
                            end;
                            if wounds < 0 then wounds := 0;
                            ShowStats;
                            if Random(10)>=6 then stepFinished := true;
                        end else begin
                            StatusLine2(s_CAN_USE_ONLY);
			    //Write( char(byte(itemSymbols[6])+128),' ',
			    //       char(byte(itemSymbols[7])+128) );
			    Write ( '"', itemSymbols[6], '" "', itemSymbols[7], '"');
			    Pause(100);
			    //ReadKey;
                        end;
                end else stepFinished := true;
            until stepFinished or not HasAnythingToUse;
    end;
    Inc(moves);
end;


// *********************************** MAIN PROGRAM

begin
    ClrScr;
    write ( 'Old Mansion C64, V' );
    writeln ( version );
    pause ( 200 );

    BasicRomOff();
    DisableShiftCommodore;
    BorderColor := 0;
    //BackgroundColor := 0;
    {msx.player := pointer(RMT_PLAYER);
    msx.modul := pointer(RMT_MODULE);}
    InitIO;
    Randomize;
{    SetIntVec(iDLI, @Dli);
    SetIntVec(iVBL, @Vbl);}
    //unDEF ( pointer(SID_SONG_RC), pointer(SID_SONG) );  // decompress music
    repeat
	StartMusic(0);
	InitGraph(2, VGAHi, '');
	//ClearGraph;
	TitleScreen;
	//pause(100);
	//readkey;
	ScreenOff();
	WaitFrame();
	InitGraph(0);
	//pause(100);
	//readkey;
	ScreenOff();
	SetUpVicText();
	ScreenOff();
	WaitFrame();
	ScreenOn();

	
	//pause(100);
	//readkey;
	//pause(100);
	
        ShowManual;
        
	StopMusic;
        PaintBoard;
        //readkey;

        //StartMusic(7);
	StartMusic(1);

        gameEnded := false;
        while not gameEnded do
            MakeMove;
	StopMusic;
    until false;
    //Close(fscr);
    TextMode(0);
    BasicRomOn();
end.
