

// https://sta.c64.org/cbm64mem.html

procedure BasicRomOn(); assembler;
asm
        //lda 1
	//ora 3
        lda #31
	sta 1
end;


procedure BasicRomOff(); assembler;
asm
	//lda 1
	//and $fe
        lda #30
	sta 1
end;

procedure ScreenOnOff(); assembler;
asm
        lda $d011
        eor #%00010000
        sta $d011
end;


procedure ScreenOn(); assembler;
asm
        lda $d011
        ora #%00010000
        sta $d011
end;


procedure ScreenOff(); assembler;
asm
        lda $d011
        and #%11101111
        sta $d011
end;


procedure DisableShiftCommodore; assembler;
asm
        lda 657
        ora $80
        sta 657
end;

(*
//
// Switch bank in VIC-II
//
// Args:
//    bank: bank number to switch to. Valid values: 0-3.
//
procedure SwitchVICBank(bank : byte);
    //
    // The VIC-II chip can only access 16K bytes at a time. In order to
    // have it access all of the 64K available, we have to tell it to look
    // at one of four banks.
    //
    // This is controller by bits 0 and 1 in $dd00 (PORT A of CIA #2).
    //
    //  +------+-------+----------+-------------------------------------+
    //  | BITS |  BANK | STARTING |  VIC-II CHIP RANGE                  |
    //  |      |       | LOCATION |                                     |
    //  +------+-------+----------+-------------------------------------+
    //  |  00  |   3   |   49152  | ($C000-$FFFF)*                      |
    //  |  01  |   2   |   32768  | ($8000-$BFFF)                       |
    //  |  10  |   1   |   16384  | ($4000-$7FFF)*                      |
    //  |  11  |   0   |       0  | ($0000-$3FFF) (DEFAULT VALUE)       |
    //  +------+-------+----------+-------------------------------------+
//  ?bits=%11
//
//    ift (%%bank==0)
//     ?bits=%11
//    eli (%%bank==1)
//     ?bits=%10
//    eli (%%bank==2)
//     ?bits=%01
//    eli (%%bank==3)
//     ?bits=%00
//    eif
//
var bits : byte;
begin
    bits := %11;
    case bank of
      1	: bits := %10;
      2	: bits := %01;
      3	: bits := %00;
    end;
    asm;
    //
    // Set Data Direction for CIA #2, Port A to output
    //
    lda $dd02
    and #%11111100  // Mask the bits we're interested in.
    ora #$03        // Set bits 0 and 1.
    sta $dd02

    //
    // Tell VIC-II to switch to bank
    //
    lda $dd00
    and #%11111100
    ora #?bits
    sta $dd00
    end;
end;
*)
