
#
# Makefile to build OldMansion with MadPascal and Mads:
#   https://mads.atari8.info/
#   https://github.com/tebe6502/Mad-Pascal
#   https://github.com/tebe6502/Mad-Assembler
#
# MadPascal Atari usage:
#   $ mp filename.pas -ipath:<MadPascalPath>\lib
#   $ mads filename.a65 -x -i:<MadPascalPath>\base
#
# Tested with versions:
#   MadPascal 1.6.9
#   Mads      2.1.5
#

MAD_PASCAL_PATH = /opt/retro/madpascal
MAD_PASCAL_LIB  = $(MAD_PASCAL_PATH)/lib
MAD_PASCAL_BASE = $(MAD_PASCAL_PATH)/base

PRG = old_mansion stary_dom

#all: old_mansion.prg stary_dom.prg
all: old_mansion.prg

old_mansion.prg: old_mansion.a65
	mads $< -x -i:$(MAD_PASCAL_BASE) -o:old_mansion.prg

stary_dom.prg: stary_dom.a65
	mads $< -x -i:$(MAD_PASCAL_BASE) -o:stary_dom.prg


old_mansion.a65: starydom.pas const.inc io.inc lang_en.inc charset.pas \
              debug.pas \
              deflate.pas \
              music.pas \
              fonts/starydom-c64.fnt.deflate \
              music/old_mansion_music.bin \
              title/title_en.deflate
	cp -v lang_en.inc lang.inc
	mp -diag $< -t c64 -ipath:$(MAD_PASCAL_LIB) -o:old_mansion.a65
	rm -v lang.inc

stary_dom.a65: starydom.pas const.inc io.inc lang_pl.inc charset.pas \
              debug.pas \
              deflate.pas \
              music.pas \
              fonts/starydom-c64.fnt.deflate \
              music/old_mansion_music.bin \
              title/title_pl.deflate
	cp -v lang_pl.inc lang.inc
	mp -diag $< -t c64 -ipath:$(MAD_PASCAL_LIB) -o:stary_dom.a65
	rm -v lang.inc


#$(TARGETS): %.prg: %.a65
#        mads $< -x -i:$(MAD_PASCAL_BASE) -o:$@

#$(OBJ): %.a65: %.pas
#       $(CC) -c $(CFLAGS) $< -o $@
#        mp -t c64 $< -ipath:$(MAD_PASCAL_LIB)
#       mp $< -ipath:$(MAD_PASCAL_LIB)


#oldmansion.xex: starydom.a65
##starydom.obx: starydom.a65
#	mads $< -x -i:$(MAD_PASCAL_BASE) -o:oldmansion.xex


fonts/starydom-c64.fnt.deflate:
	cd fonts ; make

music/old_mansion_music.bin.deflate:
	cd music ; make

title/title_pl.deflate title/title_en.deflate:
	cd title ; make

clean:
	rm -fv stary_dom.prg stary_dom.a65 stary_dom.lst stary_dom.txt \
           old_mansion.prg old_mansion.a65 old_mansion.lst old_mansion.txt \
           lang.inc
	cd fonts ; make clean
	cd music ; make clean
	cd title ; make clean

.PHONY: clean
