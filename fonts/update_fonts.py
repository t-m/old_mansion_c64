#!/usr/bin/env python3

import sys
from struct import unpack


def read_fonts():
    #print (sys.argv)

    with open('starydom4.fnt','rb') as f:
        atari_fonts_bytes = f.read()
    #print(atari_fonts_bytes)

    with open(sys.argv[1],'rb') as f:
        c64_load_offset = f.read(2)
        c64_fonts_bytes = f.read()
    return atari_fonts_bytes, c64_load_offset, c64_fonts_bytes


def convert_bytes_to_font_list(data):
    #    info = [data[i:i+2] for i in range(0, len(data), 2)]
    fonts = [data[i:i+8] for i in range(0, len(data), 8)]
    return fonts
    
def update_c64_fonts(atari_fonts,c64_fonts):
    atari_to_c64 = [
        # c64 font index: atari font index

        # sciany
        (107, 65),   # sciana zewn. lewa ( |- )
        (115, 68),   # sciana zewn. prawa ( -| )
        (112, 81),   # sciana zewn. lewy gorny rog
        (125, 67),   # sciana zewn. prawy dolny rog
        (109, 90),   # sciana zewn. lewy dolny rog
        (110, 69),   # sciana zewn. prawy gorny rog

        (124, 82),   # border wall horiz.
        (127, 124),  # border wall vert.

        (91, 83),    # krzyzyk (sciany wewn.)
        (113, 88),   # sciana zewn. dolna ( _|_ )
        (114, 87),   # sciana zewn. gorna ( _|_ )

        # przejscia / drzwi / cienkie sciany
        ( 93, 92 ),  # thin wall vertical
        ( 64, 127 ), # thin wall horizontal

        (92,  93),   # drzwi vert.
        (104, 95),   # drzwi horiz.

        (126, 94),   # przejscie vert.
        (123, 91),   # przejscie horiz.

        # specjalne
        (122, 6),   # player
        (94, 62),   # exit (left tile)
        (95, 63),   # exit (right tile) in game
        (96, 61),   # exit (right tile) in manual / symbol list

        # wnetrza pomieszczen
        ( 102, 126),   # brak podlogi
        ( 105, 125 ),  # ciemno

        # przedmioty (ZDEFINIOWANE W CHARSET ALE NIE UZYWANE W PROGRAMIE!)
        ( 97, 2 ),  # key
        ( 98, 3 ),  # medkit
    ]

    print (c64_fonts)
    print (atari_to_c64)

    for fconv in atari_to_c64:
        print ( fconv )
        c64_fonts[ fconv[0] ] = atari_fonts [ fconv[1] ]

def write_c64_fonts ( fonts, offset ):
    with open('stary_dom.fnt_c64','wb') as f:
        #f.write(offset)
        for font in fonts:
            f.write(font)

def write_c64_fonts_prg ( fonts, offset ):
    with open( 'stary_dom.fnt_c64.prg','wb') as f:
        f.write(offset)
        for font in fonts:
            f.write(font)

    
atari_fonts_bytes, c64_load_offset, c64_fonts_bytes = read_fonts()

#print(c64_fonts_bytes)
#print(c64_load_offset, type(c64_load_offset) )

atari_fonts = convert_bytes_to_font_list(atari_fonts_bytes)
c64_fonts   = convert_bytes_to_font_list(c64_fonts_bytes)

update_c64_fonts ( atari_fonts, c64_fonts )

write_c64_fonts(c64_fonts, c64_load_offset)
write_c64_fonts_prg(c64_fonts, c64_load_offset)
