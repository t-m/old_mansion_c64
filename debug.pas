
const control_chars : array [ 0 .. 31 ] of byte = (
    5, //$05: Changes the text color to white.
    8, //$08: Disables changing the character set using
       //     the SHIFT + Commodore key combination.
    9, //$09: Enables changing the character set using
       //     the SHIFT + Commodore key combination.
    13, {$0D: Carriage return; next character will go in the first column
	      of the following text line. As opposed to traditional
	      ASCII-based system, no LINE FEED character needs to be sent
	      in conjunction with this Carriage return character in
	      the PETSCII system. }
    14, //$0E: Select the lowercase/uppercase character set.
    17, { $11: Cursor down: Next character will be printed in subsequent
	       column one text line further down the screen. }
    18, //$12: Reverse on: Selects reverse video text.
    19, { $13: Home: Next character will be printed in the upper left-hand
	       corner of the screen. }
    20, //$14: Delete, or "backspace"; erases the previous character and moves the cursor one character position to the left.
    28, //$1C: Changes the text color to red.
    29, //$1D: Advances the cursor one character position without printing anything.
    30, //$1E: Changes the text color to green.
    31, //$1F: Changes the text color to blue.
    129, //$81: Changes the text color to orange.
    141, //$8D: Same action as Carriage return (13/$0D).
    142, //$8E: Select the uppercase/semigraphics character set.
    144, //$90: Changes the text color to black.
    145, //$91: Cursor up: Next character will be printed in subsequent column one text line further up the screen.
    146, //$92: Reverse off: De-selects reverse video text.
    147, //$93: Clears screen of any text, and causes the next character to be printed at the upper left-hand corner of the text screen.
    148, //$94: Insert: Makes room for extra characters at the current cursor position, by "pushing" existing characters at that position further to the right.
    149, //$95: Changes the text color to brown.
    150, //$96: Changes the text color to light red.
    151, //$97: Changes the text color to dark grey.
    152, //$98: Changes the text color to medium grey.
    153, //$99: Changes the text color to light green.
    154, //$9A: Changes the text color to light blue.
    155, //$9B: Changes the text color to light grey.
    156, //$9C: Changes the text color to purple.
    157, //$9D: Moves the cursor one character position backwards, without printing or deleting anything.
    158, //$9E: Changes the text color to yellow.
    159  //$9F: Changes the text color to cyan.
);

procedure ShowCharacters;
var
    i, j	    : byte;
    is_control_char : boolean;
begin
    ClrScr;
    for i:=0 to 255 do
    begin
	is_control_char := false;
	for j:=0 to 31 do
	    if i = control_chars[j] then
	    begin
		is_control_char := true;
		break;
	    end;
	if is_control_char then
	    continue;
	Write(char(i));
	readkey;
    end;
end;
