

procedure StartMusic(n : byte); assembler;
asm
    sei

    // init goat tracker music
    lda n
    jsr $7900

    // setup irq
    //sei
    lda #<frame_int_handler
    sta $0314
    lda #>frame_int_handler
    sta $0315
    cli

    rts

frame_int_handler
    jsr $7903      // play goattracker music
int_orig
    jmp $ea31
end;

procedure tmp; assembler;
asm
frame_int_handler2
    lda #1
    sta $d020
    ldx #0
loop
    inx
    cpx #$6f
    bne loop
    lda #0
    sta $d020
    jsr $7900
int_orig2
    jmp $ea31
end;


procedure StopMusic; assembler;
asm
    lda $d418
    and $f0
    sta $d418
    sei
    lda #$31
    sta $0314
    lda #$ea
    sta $0315
    cli
end;
