
/*

  PUT CHAR

*/

.proc	@putchar (.byte a) .reg

chrout	= $ffd2			;kernel character output sub

;	cmp #64
;	scc
;	eor #%00100000

; exchange A-Z <-> a-z (to convert ASCII -> PETSCII - required as ASCII is Mad Pascal
; source code encoding, and such strings must be converted to use with C64 Kernal
; subroutines)
; Note: this code below change only only the letters!, while the code above
; changed all chars above 64 (what made impossible to use eg. special screen
; codes, semigraphics etc.)
	cmp #64
	bcc putcharend

	cmp #122
	bcs putcharend

	cmp #91
	bcc putchareor

	cmp #96
	bcc putcharend
putchareor
	eor #%00100000
putcharend
	jmp chrout
.endp
