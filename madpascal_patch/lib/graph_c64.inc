
procedure SetActiveBuffer(var a: TDisplayBuffer);
(*
@description:

*)
begin

 VideoRam := pointer(a.bp);
// savmsc := VideoRam;

end;


procedure SetDisplayBuffer(var a: TDisplayBuffer);
(*
@description:
Set video buffer which is displayed
*)
begin
//	sdlstl := a.dl;
//	dlistl := sdlstl;
end;


function NewDisplayBuffer(var a: TDisplayBuffer; mode, bound: byte): TDisplayBuffer;
(*
@description:
Initialize new graphical buffer
*)
begin

// ramtop := bound;

// InitGraph(mode);

// a.dl := sdlstl;
// a.bp := savmsc;

end;


procedure SwitchDisplayBuffer(var a,b: TDisplayBuffer);
(*
@description:
Switch graphical buffer between A <> B
*)
var tmp: TDisplayBuffer;
    x, y: pointer;
    l: word;
begin

{
 tmp:=b;

 b:=a;
 a:=tmp;

 SetDisplayBuffer(a);
 SetActiveBuffer(b);

 x:=Scanline(WIN_TOP);

 y:=Scanline(WIN_BOTTOM);
 l:=word(y);
 y:=Scanline(WIN_TOP);

 dec(l, word(y));

 fillchar(x, l, 0);
}

end;


procedure SetColor(color: byte); assembler;
(*
@description:
Sets the foreground color to Color
*)
asm
	lda MAIN.SYSTEM.GraphMode
	cmp #VGAHi
	beq HiRes
	cmp #VGAMed
	beq Multi

	jmp @exit

Multi	lda color
	and #$03
	cmp #1
	beq c1_m
	cmp #2
	beq c2_m
	cmp #3
	beq c3_m

c0_m
	mva #{and#} @putpixel.msk

	.put %11000000^$ff, %00110000^$ff, %00001100^$ff, %00000011^$ff
	:4 mva #.get[#] @putpixel.color+#

	jmp @exit

c1_m
	.put %01000000, %00010000, %00000100, %00000001
	:4 mva #.get[#] @putpixel.color+#

	bne skp

c2_m
	.put %10000000, %00100000, %00001000, %00000010
	:4 mva #.get[#] @putpixel.color+#

	bne skp

c3_m
	.put %11000000, %00110000, %00001100, %00000011
	:4 mva #.get[#] @putpixel.color+#

skp
	mva #{ora#} @putpixel.msk

	jmp @exit

; ------------------------------------

HiRes	lda color
	and #1
	bne c1_h
c0_h
	mva #{and#} @putpixel.msk

	.put $80^$ff,$40^$ff,$20^$ff,$10^$ff,$08^$ff,$04^$ff,$02^$ff,$01^$ff
	:8 mva #.get[#] @putpixel.color+#

	jmp @exit

c1_h
	mva #{ora#} @putpixel.msk

	.put $80,$40,$20,$10,$08,$04,$02,$01
	:8 mva #.get[#] @putpixel.color+#
end;


procedure ClearGraph();
begin
    fillchar( VideoRam,  25 * 320, 0 );
    fillchar( ScreenRAM, 25 * 40,  32 );	// 32 = WHITE * 16 + BLUE
    //SetColor(0);
end;


//procedure InitGraph(mode: byte); overload;
//procedure InitGraph(driver,            // for c64 - vic bank(!)
//                    mode    : byte;
//                    dev     : PString) ; overload;
procedure _initgraph2( mode,
		      vic_bank       : byte;
		      vic_base,
		      screen_memory,
 		      bitmap_address : word );
(*
@description:
Init graphics mode
*)
begin
  asm
    //vic_bank=1
    //vic_bank = driver 

    //vic_base = $4000 * vic_bank		// A VIC-II bank indicates a 16K region
    //screen_memory = $1000 + vic_base
    //bitmap_address = $2000 + vic_base

    mwa #bitmap_address VideoRAM
    mwa #screen_memory ScreenRAM

    txa:pha

    lda mode
    sta MAIN.SYSTEM.GraphMode

    cmp #VGAHi
    beq _hi
    cmp #VGAMed
    beq _med

_0
    jsr $FF81	; SCINIT. Initialize VIC
    jsr $FF84	; IOINIT. Initialize CIAs, SID volume

    lda <40
    ldx >40
    ldy #25

    bne skp

_med

    SwitchVICBank(vic_bank)
    SetMulticolorBitmapMode()
    SetScreenMemory(screen_memory - vic_base)
    SetBitmapAddress(bitmap_address - vic_base)

    lda <160
    ldx >160
    ldy #200

    bne skp

_hi
    SwitchVICBank(vic_bank)
    SetHiresBitmapMode()
    SetScreenMemory(screen_memory - vic_base)
    SetBitmapAddress(bitmap_address - vic_base)

    lda <320
    ldx >320
    ldy #200

skp
    @SCREENSIZE

    pla:tax

 end;

// fillchar(VideoRam, 25*320, 0);
// fillchar(ScreenRAM, 25*40, 32);	// 32 = WHITE * 16 + BLUE
//  ClearGraph;

 SetColor(0);

end;


procedure SwitchVICBank( vic_bank_bits : byte ); assembler;
asm
    //
    // The VIC-II chip can only access 16K bytes at a time. In order to
    // have it access all of the 64K available, we have to tell it to look
    // at one of four banks.
    //
    // This is controller by bits 0 and 1 in $dd00 (PORT A of CIA #2).
    //
    //  +------+-------+----------+-------------------------------------+
    //  | BITS |  BANK | STARTING |  VIC-II CHIP RANGE                  |
    //  |      |       | LOCATION |                                     |
    //  +------+-------+----------+-------------------------------------+
    //  |  00  |   3   |   49152  | ($C000-$FFFF)*                      |
    //  |  01  |   2   |   32768  | ($8000-$BFFF)                       |
    //  |  10  |   1   |   16384  | ($4000-$7FFF)*                      |
    //  |  11  |   0   |       0  | ($0000-$3FFF) (DEFAULT VALUE)       |
    //  +------+-------+----------+-------------------------------------+

    //
    // Set Data Direction for CIA #2, Port A to output
    //
    lda $dd02
    and #%11111100  // Mask the bits we're interested in.
    ora #$03        // Set bits 0 and 1.
    sta $dd02

    //
    // Tell VIC-II to switch to bank
    //
    lda $dd00
    and #%11111100
    ora vic_bank_bits
    sta $dd00
end;



//
// Switch location of screen memory.
//
// Args:
//   address: Address relative to current VIC-II bank base address.
//            Valid values: $0000-$3c00. Must be a multiple of $0400.
//
procedure SetScreenMemory ( address : word );
    //
    // The most significant nibble of $D018 selects where the screen is
    // located in the current VIC-II bank.
    //
    //  +------------+-----------------------------+
    //  |            |         LOCATION*           |
    //  |    BITS    +---------+-------------------+
    //  |            | DECIMAL |        HEX        |
    //  +------------+---------+-------------------+
    //  |  0000XXXX  |      0  |  $0000            |
    //  |  0001XXXX  |   1024  |  $0400 (DEFAULT)  |
    //  |  0010XXXX  |   2048  |  $0800            |
    //  |  0011XXXX  |   3072  |  $0C00            |
    //  |  0100XXXX  |   4096  |  $1000            |
    //  |  0101XXXX  |   5120  |  $1400            |
    //  |  0110XXXX  |   6144  |  $1800            |
    //  |  0111XXXX  |   7168  |  $1C00            |
    //  |  1000XXXX  |   8192  |  $2000            |
    //  |  1001XXXX  |   9216  |  $2400            |
    //  |  1010XXXX  |  10240  |  $2800            |
    //  |  1011XXXX  |  11264  |  $2C00            |
    //  |  1100XXXX  |  12288  |  $3000            |
    //  |  1101XXXX  |  13312  |  $3400            |
    //  |  1110XXXX  |  14336  |  $3800            |
    //  |  1111XXXX  |  15360  |  $3C00            |
    //  +------------+---------+-------------------+
    //
var
    bits : byte;
begin
    bits := (address div $0400) shl 4;
    //bits := (address shr 10) shl 4; // does not work, shl/r seem to work only for bytes
    asm
        lda $d018
        and #%00001111
        ora bits
        sta $d018
    end;
end;


//
// Enter text mode
//
procedure SetTextMode; assembler;
asm
    // Clear extended color mode (bit 6) and bitmap mode (bit 5)
    lda $d011
    and #%10011111
    sta $d011

    // Clear multi color mode (bit 4)
    lda $d016
    and #%11101111
    sta $d016
end;



//
// Enter hires bitmap mode (a.k.a. standard bitmap mode)
//
procedure SetHiresBitmapMode; assembler;
asm
    //
    // Clear extended color mode (bit 6) and set bitmap mode (bit 5)
    //
    lda $d011
    and #%10111111
    ora #%00100000
    sta $d011

    //
    // Clear multi color mode (bit 4)
    //
    lda $d016
    and #%11101111
    sta $d016
end;


//
// Enter multicolor bitmap mode
//
procedure SetMulticolorBitmapMode; assembler;
asm
    //
    // Clear extended color mode (bit 6) and set bitmap mode (bit 5)
    //
    lda $d011
    and #%10111111             // t-w sprawdzic czy nie blad, czy nie powinno byc set (ora ...)
    ora #%00100000
    sta $d011

    //
    // Set multi color mode (bit 4)
    //
    lda $d016
    ora #%00010000
    sta $d016
end;


//
// Set location of bitmap.
//
// Args:
//    address: Address relative to VIC-II bank address.
//             Valid values: $0000 (bitmap at $0000-$1FFF)
//                           $2000 (bitmap at $2000-$3FFF)
//
procedure SetBitmapAddress( address : word );// assembler;
    //
    // In standard bitmap mode the location of the bitmap area can
    // be set to either BANK address + $0000 or BANK address + $2000
    //
    // By setting bit 3, we can configure which of the locations to use.
    //
(*
asm
    lda $d018

    //ift %%address == $0000
    //    and #%11110111
    //eli %%address == $2000
    ///    ora #%00001000
    //eif
    ldx #>address
    cpx #$20
    beq bmp2000
    and #%11110111
    jmp storebmpadr
bmp2000
    ora #%00001000

storebmpadr
    sta $d018
 *)
var d018 : byte;
begin
    d018 := peek( $d018 );
    if address = $2000 then
	d018 := d018 or %00001000
    else
	d018 := d018 and %11110111;
    poke ( $d018, d018 );
end;

procedure VICReset_; assembler;
asm
    jsr $ff81	; SCINIT. Initialize VIC
    jsr $ff84	; IOINIT. Initialize CIAs, SID volume

    //lda <40
    //ldx >40
    //ldy #25
    //@SCREENSIZE
end;

procedure SetTextMode_; assembler;
asm
//    jsr $ff81	; SCINIT. Initialize VIC
//    jsr $ff84	; IOINIT. Initialize CIAs, SID volume
    lda <40
    ldx >40
    ldy #25
    @SCREENSIZE
end;


procedure SetVGAMedMode_; assembler;
asm
    lda <160
    ldx >160
    ldy #200
    @SCREENSIZE
end;

procedure SetVGAHiMode_; assembler;
asm
    lda <320
    ldx >320
    ldy #200
    @SCREENSIZE
end;


//procedure InitGraph(mode: byte); overload;
//procedure InitGraph(driver,            // for c64 - vic bank(!)
//                    mode    : byte;
//                    dev     : PString) ; overload;
procedure _initgraph( mode,
		      vic_bank       : byte;
		      vic_base,
		      screen_memory,
 		      bitmap_address : word );
(*
@description:
Init graphics mode
*)
var vic_bank_bits : byte;
begin
    VideoRAM  := pointer(bitmap_address);
    ScreenRAM := pointer(screen_memory);
    //MAIN.SYSTEM.GraphMode := mode;
    GraphMode := mode;

    if mode = 0 then
    begin
	SetTextMode;
	SetTextMode_;
    end else begin
        case vic_bank of
	  0 : vic_bank_bits := %11;
	  1 : vic_bank_bits := %10;
	  2 : vic_bank_bits := %01;
	  3 : vic_bank_bits := %00;
	end;
	SwitchVICBank( vic_bank_bits );
	if mode = VGAMed then
	    SetMulticolorBitmapMode()
	else
	    SetHiresBitmapMode();
	SetScreenMemory( screen_memory - vic_base );
	SetBitmapAddress( bitmap_address - vic_base );
	if mode = VGAMed then
	begin
	    //SetMulticolorBitmapMode();
	    SetVGAMedMode_;
	end else begin   /// VGAHi
	    //SetHiresBitmapMode();
	    SetVGAHiMode_;
	end
    end;
    // fillchar(VideoRam, 25*320, 0);
    // fillchar(ScreenRAM, 25*40, 32);	// 32 = WHITE * 16 + BLUE
    ClearGraph;
    SetColor(0);
end;


//procedure InitGraph(mode: byte); overload;
procedure InitGraph(driver,            // for c64 - vic bank(!), valid values: 0, 1, 2, 3
                    mode    : byte;
                    dev     : PString) ; overload;
(*
@description:
Init graphics mode
*)
var
    vic_bank       : byte;
    vic_base,
    screen_memory,
    bitmap_address : word;    
begin
    vic_bank       := driver;
    vic_base       := $4000 * vic_bank;  // A VIC-II bank indicates a 16K region
    //screen_memory  := $1000 + vic_base;  // does not work for bank 3, at $9000 VIC always sees
                                           // character ROM(!),
                                           // https://codebase64.org/doku.php?id=base:vicii_memory_organizing
    screen_memory  := vic_base;
    bitmap_address := $2000 + vic_base;
    _initgraph( mode, vic_bank, vic_base, screen_memory, bitmap_address );
end;


procedure InitGraph(mode: byte); overload;
(*
@description:
Init graphics mode
*)
begin
    InitGraph( 2,          // default VIC bank 2 / $8000
               mode, '' );
end;


{
procedure InitGraph(driver, mode: byte; dev: PString); overload;
(*
@description:
Init graphics mode
*)
begin

 InitGraph(mode);

end;
}


procedure SetBkColor(color: byte); assembler;
(*
@description:
Sets the background color to Color
*)
asm

 sta MAIN.C64.Backgroundcolor0

end;


procedure PutPixel(x,y: smallint); assembler; overload;
(*
@description:
Puts a point at (X,Y) using color Color
*)
asm
	lda y+1
	bmi stop
	cmp MAIN.SYSTEM.ScreenHeight+1
	bne sk0
	lda y
	cmp MAIN.SYSTEM.ScreenHeight
sk0
	bcs stop

	lda x+1
	bmi stop
	cmp MAIN.SYSTEM.ScreenWidth+1
	bne sk1
	lda x
	cmp MAIN.SYSTEM.ScreenWidth
sk1
	bcs stop

; -----------------------------------------

	lda MAIN.SYSTEM.GraphMode
	cmp #VGAHi
	beq HiRes

MedRes
	lda X
	and #3
	tay
	lda @putpixel.color,y
	sta @putpixel.msk+1

	ldy Y

	lda X
	and #%11111100

	asl @
	rol X+1

	add @putpixel.ladr,y
	sta :bp2
	lda X+1
	adc @putpixel.hadr,y
	adc VideoRam+1
	sta :bp2+1

	jmp @putpixel

stop	rts

; -----------------------------------------

HiRes
	lda X
	and #7
	tay
	lda @putpixel.color,y
	sta @putpixel.msk+1

	ldy Y

	lda X
	and #%11111000
	add @putpixel.ladr,y
	sta :bp2
	lda X+1
	adc @putpixel.hadr,y
	adc VideoRam+1
	sta :bp2+1

	jmp @putpixel

end;


procedure PutPixel(x,y: smallint; color: byte); overload;
(*
@description:
Puts a point at (X,Y) using color Color
*)
begin

 SetColor(color);

 PutPixel(x,y);

end;


function GetPixel(x,y: smallint): byte; assembler;
(*
@description:
Return color of pixel
*)
asm

end;


procedure LineTo(x, y: smallint);
(*
@description:
Draw a line starting from current position to a given point
*)
var x1, y1, dx, dy, fraction, stepx, stepy: smallint;

begin
    x1 := CurrentX;
    y1 := CurrentY;

    if x1<0 then x1:=0;
    if y1<0 then y1:=0;

    if x<0 then x:=0;
    if y<0 then y:=0;

    dy := y - y1;
    dx := x - x1;

    if (dy < 0) then begin dy := -dy; stepy := -1 end else stepy := 1;
    if (dx < 0) then begin dx := -dx; stepx := -1 end else stepx := 1;
    dy := dy + dy;	// dy is now 2*dy
    dx := dx + dx;	// dx is now 2*dx

    PutPixel(x1,y1);

    if (dx > dy) then begin

        fraction := dy shl 1 - dx;

        while (x1 <> x) do begin
           if (fraction >= 0) then begin
               inc(y1, stepy);
               dec(fraction,dx);		// same as fraction -= 2*dx
           end;
           inc(x1, stepx);
           inc(fraction, dy);			// same as fraction -= 2*dy

	   PutPixel(x1, y1);
        end;

     end else begin

        fraction := dx shl 1 - dy;

        while (y1 <> y) do begin
           if (fraction >= 0) then begin
               inc(x1, stepx);
               dec(fraction, dy);
           end;
           inc(y1, stepy);
           inc(fraction, dx);

           PutPixel(x1, y1);
        end;
     end;

 CurrentX := x;
 CurrentY := y;

end;


procedure CloseGraph; assembler;
(*
@description:

*)
asm

end;

