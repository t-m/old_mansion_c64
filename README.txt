
"Old mansion" is a simple turn-based, rogue-like (adventure) game where
we control actions of a hero that has found himself in an old/haunted mansion,
and he has to traverse the rooms to reach the exit. Along the way, we must
gather weapons, items and valuables, and deal with random events and encounters
(monsters, dark rooms, item damage etc.). Managing energy level, ensuring
having the best possible weapon and items required for traversing locations -
all that is necessary to succeed.

This version is a Commodore 64 port of a recent remake of "Stary Dom"
("Old mansion"), a game for 8-bit Atari computers, written in BASIC and
published first in Bajtek computer magazine (issue 4/87, page 6, see link 1.) -
so years ago in Poland still governed by communists.

As few years old kid, I was pretty intrigued by the big, mysterious game
program that was shown to me by my primary school friend (who was the 3rd
person in our class owning a computer - an Atari 65XE). We attempted to type
it in - but, due to the length and complexity (special characters, unreadable
code in some places...), it required too much time for our kids' patience.

A bit later, as an owner of a C64, I was still curious about the game and,
of course, wanted to have it on my machine. But with limited technical
information available, not knowing even how the game really looks like and
lacking knowledge about programming Ataris - made porting it at that time
beyond possibility...

After some years, having some fun with retro-computing, I have found Bocianu's
remake for Atari (2.). It was written in Mad-Pascal (3.) what made it way
more likely to be ported elsewhere. And that became direct starting point for
making this C64 version.

Note that compiling the code with Mad Pascal requires also Mad Assembler (4.).

The port is still kind of a prototype. It requires patching Mad-Pascal's
library (for C64), as I had to reorganize screen configuration (not available
in Mad-Pascal, at least up to version 1.6.9), and also change memory
configuration (disable BASIC ROM, change charset etc.). The modified modules
are stored in the repository (maybe in some form they will go to Mad Pascal...).

So far only the English version is ported. Polish (so, in fact - the original)
version require more updates of the charset etc. It may appear at some point
(not 100% sure though...).

Anyway - the English version is playable, so have fun ;-)

t-w (Tomasz Wolak)


------------------------------------------------------------------------

Credits:
- The original game was written by Wojciech Zientara and published in
  Bajtek computer magazine (see 1.)
- The Atari remake in Mad Pascal was developed by Bocianu (2.)

------------------------------------------------------------------------

The requirements for building (minimal versions; however, due to frequent
big changes done in Mad-Pascal, some changes/updates may be needed to build
using newer versions!):
- Mad Pascal 1.6.9 (with patches for c64, stored in madpascal_patch/)
- Mad Assembler 2.1.5
- zopfli (for compressing intro screen and charset with deflate method)

------------------------------------------------------------------------

Technical notes:

This version is a direct port of Bocianu's remake, so many things are common.
Some details:
- most of the code comes from that remake, with all necessary add-ons and
  changes to make it work on C64
- the logo screen was converted (and slightly modified, adding info about
  the c64 port)
- the modified Atari fonts (character graphics) were used to patch the c64's
  second character set (with upper and lower case characters)
- the original music (for Atari's POKEY, created with RMT, the Raster Music
  Tracker) was recreated as a Goat Tracker module (using instruments found
  in the examples). While the tunes preserve the orig. themes quite closely,
  they sound differently, more "SID" (some may like it or not ;-)

Some changes vs. Atari version:
- The main game screen is done in greyscale, it may be changed/improved
  in the future (or not - I like it this way ;-)
- Some minor changes are done in screen layout, ie. items are listed
  with full names (no need to remember which letter represents what)
- Items received are displayed with their value
- Some language details were improved (ie. the use of articles)
- A few minor changes were done in the gameplay
  - some states that do not require any player's action (nothing to choose,
    just displaying an information about an event/its result), do not require
    a keypress to continue (it is done automatically after a small delay)
- The intro screen and the custom charset are compressed using deflate
  - the procedure unDEF() from deflate.pas provided with Mad Pascal 1.6.9
    does not work on C64, due to too small buffer:
      RTBUF: $0800..$08FF
    (according to info in deflate.pas, unDEF requires a buffer of 765 bytes)
    -> a corrected version is provided along with the game sources.
- Screen initialization and configuration as well as memory map had to be
  customized (BASIC ROM off, VIC bank, bitmap and charset addresses etc.)
  For this the InitGraph() from Mad-Pascal was heavily modified and many
  additional procedures/functions were added (maybe some of these can be
  incorporated into Mad Pascal...).

------------------------------------------------------------------------

References:

1. https://archive.org/details/bajtek198704/page/n5/mode/2up

2. http://bocianu.atari.pl/blog/starydom
   https://gitlab.com/bocianu/oldmansion

3. https://github.com/tebe6502/Mad-Pascal

4. https://mads.atari8.info/
   https://github.com/tebe6502/Mad-Assembler
