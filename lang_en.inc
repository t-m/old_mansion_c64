{$r lang_en.rc}

const
    monsters: array [0..MONSTERS_COUNT - 1] of TString = (
        'BAT',
        'RAT',
        'LIZARD',
        'SPIDER',
        'DOG',
        'SNAKE',
        'KRONK',
        'GNOME',
        'GOBLIN',
        'ZOMBIE',
        'WARG',
        'GHOST',
        'ELF',
        'WEREWOLF',
        'WITCH',
        'ORC',
        'GARGOYLE',
        'TROLL',
        'MAGE',
        'VAMPIRE',
        'NIGHTMARE',
        'MINOTAUR',
        'CYCLOPE',
        'GIANT',
        'HYDRA',
        'DEMON',
        'MEDUSA',
        'DRAGON',
        'DEVIL',
        'PHOENIX'
     );
    weapons: array [0..WEAPONS_COUNT - 1] of TString = (
        'FORK',
        'KNIFE',
        'RAZOR',
        'DAGGER',
        'SABER',
        'LANCE',
        'JAVELIN',
        'SCIMITAR',
        'AXE',
        'SWORD' 
    );
    treasures: array [0..TREASURES_COUNT - 1] of TString = (
        'COPPER',
        'SILVER',
        'GOLD',
        'PLATINUM',
        'DIAMONDS' 
    );
    items: array [0..ITEMS_COUNT - 1] of TString = (        
        'HAMMER',
        'LANTERN',
        'KEY',
        'PLANK',
        'FOOD',
        'DRINK',
        'BANDAGE',
        'MEDICINE',
        'PASSWORD' 
    );
    itemSymbols: array [0..ITEMS_COUNT - 2] of char =
        ( 'H', 'L', 'K', 'P', 'F', 'D', 'B', 'M');
    passwords: array [0..PASSWORDS_COUNT - 1] of TString =
        ('FISH','EGGS','BYTE','TREE','CLAN','CULT','MOON','BULB','LASH');

const
    k_YES     = char('Y');//byte('y');
    k_NO      = char('N');
    k_LEFT    = char('L');
    k_RIGHT   = char('R');
    k_UP      = char('U');
    k_DOWN    = char('D');
    k_FIGHT   = char('F');
    k_RANSOM  = char('B');
    k_TAKE    = char('T');
    k_LEAVE   = char('L');
    k_REST    = char('R');
    k_MOVE    = char('M');
    k_BANDAGE = char('B');
    k_MEDKIT  = char('M');
    s_PRESS_ANY: string = 'PRESS ANY KEY';
    s_WANT_MANUAL: string = ' Do you want to read the manual (Y/N)?';
    s_ENERGY: string = 'Energy';//*;
    s_TREASURE: string = 'Treasure';//*;
    s_MOVES: string = 'Moves';//*;
    s_WOUNDS: string = 'Wounds';//*;
    s_ITEMS: string = 'Items';//*;
    s_WEAPON: string = 'Weapon';//*;
    s_ATTACK: string = 'Attack';//*;
    s_FOUND: string = 'You have found: ';
    //s_TAKE: string = 'T'*'AKE';
    //s_TAKE: string = #212'ake';
    s_TAKE: string = #18'T'#146'ake';
    s_OR: string = ' OR ';
    //s_LEAVE: string = 'L'*'EAVE';
    //s_LEAVE: string = #204'eave';
    s_LEAVE: string = #18'L'#146'eave';
    s_FOUND_PASS: string = 'You have found a password: ';
    s_REMEMBER: string = 'Remeber it.';
    s_LEAVE_WHAT: string = 'What do you want to drop?   ';
    s_DONT_HAVE: string = 'You do not have ';
    //s_WAIT: string = '  '#32'R'*'EST';
    //s_WAIT: string = '  '#$12'R'#146'EST';
    //s_WAIT: string = '  '#210'est';
    //s_WAIT: string = '  '#5'R'#159'est';
    s_WAIT: string = '  '#18'R'#146'est';
    //s_MOVE: string = 'M'*'OVE';
    //s_MOVE: string = #205'ove';
    s_MOVE: string = #18'M'#146'ove';
    //s_LEFT: string = ' '#32'L'*'EFT';
    //s_LEFT: string = ' '#204'eft';
    s_LEFT: string = ' '#18'L'#146'eft';
    //s_RIGHT: string = 'R'*'IGHT';
    //s_RIGHT: string = #210'ight';
    s_RIGHT: string = #18'R'#146'ight';
    //s_UP: string = 'U'*'P';
    //s_UP: string = #213'P';
    s_UP: string = #18'U'#146'P';
    //s_DOWN: string = 'D'*'OWN';
    //s_DOWN: string = #196'own';
    s_DOWN: string = #18'D'#146'own';
    s_DROPPED: string = 'so you dropped it.';
    s_USED: string = 'You have used the ';
    s_ATTACKED: string = 'attacked';
    s_DOOR_OPENED: string = 'The door is open, entering.';
    s_DOOR_CLOSED: string = 'The door is closed, ';
    s_KEY: string = 'KEY';
    s_BYKEY: string = 'KEY';
    s_WALL: string = 'Thin wall, ';
    s_HAMMER: string = 'HAMMER';
    s_BYHAMMER: string = 'HAMMER';
    s_ROOM_DARK: string = 'The room is dark,';
    s_LANTERN: string = 'LANTERN';
    s_BYLANTERN: string = 'LANTERN';
    s_ROOM_HOLE: string = 'There is no floor in this room,';
    s_PLANK: string = 'PLANK';
    s_BYPLANK: string = 'PLANK';
    s_EXIT_PASS: string = 'You have reached the exit. Say the password.';
    s_EXIT_PAY: string = 'Thank you! Give me the money (100$).';
    s_EXIT_LEAVE: string = 'You left the Old Mansion with ';
    s_EXIT_SCORE: string = ' to sell. Score = ';
    s_EXIT_POOR: string = 'Ouch! You do not have enough money.';
    s_EXIT_FATAL: string = 'The guardian cuts your head off.';
    s_EXIT_WRONG_PASS: string = 'Wrong! The password was: ';
    s_BUMP: string = 'You hit the wall';
    s_NO_PASARAN: string = 'Better find other way.';
    s_BACK_TO_START: string = 'You have been teleported to start.';
    s_ITEM_BROKE: array [0..7] of string = (
        'The hammer is broken, ',
        'The lantern is out of oil, ',
        'The key is rusted, ',
        'The plank decayed, ',
        'The food is rotten, ',
        'The drink dried off, ',
        'The bandage is ripped, ',
        'The medicine has expired, '
    );
    s_BROKE: string = 'You have broken ';
    s_YOU_M: string = ' by ';
    s_YOU_F: string = '';
    s_MONSTER_STR: string = 'attack ';
    s_TOO_WEAK_POOR: string = 'You are too weak and poor';
    s_TOO_WEAK: string = 'You are too weak, you must pay.';
    s_TOO_POOR: string = 'You have to fight.';
    //s_FIGHT: string = 'F'*'IGHT';
    //s_RANSOM: string = 'B'*'RIBE';
    //s_FIGHT_OR_BRIBE: string = #198'ight or '#194'ribe';
    s_FIGHT_OR_BRIBE: string = #18'F'#146'ight or '#18'B'#146'ribe';

    s_HAS_BEEN: string = ' has been';
    s_DEFEATED_F: string = '';
    s_DEFEATED_M: string = ' defeated. ';
    s_EARNED: string = 'You earn ';
    s_HAS_STR: string = ' has attack ';
    s_WANNA_USE: string = 'Do you want to use an item (Y/N)?';
    s_WHICH: string = 'Which one?       ';
    s_CAN_USE_ONLY: string = 'Now you can use only ';
    s_AND: string = 'and ';
    s_A: string ='a ';
    s_THE: string = 'The ';
    s_YOU_ARE: string = 'You are ';
    
    
function needPostfix(monster: byte): boolean;
begin
    result := false; 
end;

procedure ManualPage1;
begin
    {lmargin:=0;}
    Writeln;
    Writeln;  //                                     /
    Writeln('You have suddenly found yourself        in an old and spooky mansion.');
    Writeln('You need to get out of there.');
    Writeln;  //                                     /
    Writeln('There is only one exit and it''s guarded.The guard will let you out if you tell');
    Writeln('him the password and pay $100.');
    Writeln;  //                                     /
    Writeln('You need to collect the money (by');
    Writeln('finding the treasures) which belong to');
    Writeln('the monsters living there. You can fightthem if you have less than 5 wounds');
    Writeln('and your power is greater than 0.');
    Writeln;  //                                     /
    Writeln('You can increase your energy when you');
    Writeln('find or buy food or drinks. Wounds can');
    Writeln('be healed with medicines or bandages.');
end;
    
procedure ManualPage2;
begin
    Writeln;  //                                     /
    Writeln('If you can''t fight, you need to pay');
    Writeln('the monster for letting you out.');
    Writeln;  //                                     /
    Writeln('The password can be found somewhere');
    Writeln('in the mansion. Memorise it well!');
    Writeln;  //                                     /
    Writeln;
    Writeln('The mansion is very old. Some rooms');
    Writeln('are dark, some have no floor.');
    Writeln('You can find closed doors or thin walls');
    Writeln('so make sure to collect items.');
    Writeln;  //                                     /
    Writeln('But remember - you can only carry');
    Writeln('four items and one weapon at once.');
    Writeln;  //                                     /
    Writeln('Pick always the best weapon; it depends');
    Writeln('on your attack strength and');
    Writeln('the treasures you''ve collected.');
end;

procedure ManualPage3;
begin
    TextColor(LIGHT_GREY);
    Writeln;
    Writeln('               SYMBOLS:');
    Writeln;

    TextColor(GREY);
    //Write(char(TILE_EXIT),char(TILE_EXIT-1)); Writeln(' - exit');
    Position (10,3);
    PrintRaw(TILE_EXIT);
    PrintRaw(TILE_EXIT2_MANUAL);
    Writeln('  exit');

    Position (10,5);
    PrintRaw(TILE_DARK);
    Writeln('   dark room');

    Position (10,7);
    PrintRaw(TILE_HOLE);
    Writeln('   collapsed floor');

    Position (9,9);
    //Write(#32,char(TILE_DOOR_V));
    PrintRaw( TILE_DOOR_H, 32, TILE_DOOR_V );
    Writeln( '  closed door');

    Position (9,11);
    //Write(char(TILE_WALL_H), #32); Write(char(TILE_WALL_V)); Writeln( ' - thin wall');
    PrintRaw (TILE_WALL_H, 32, TILE_WALL_V);
    Write( '  thin wall');

    Position (10,13);  Write('H   hammer');
    Position (10,14);  Write('L   lantern');
    Position (10,15);  Write('K   key');
    Position (10,16);  Write('P   plank');
    Position (10,17);  Write('B   bandages');
    Position (10,18);  Write('M   medicines');
    
    //Writeln( char(TILE_PLAYER), ' - this is you');
    Position (10, 20);
    PrintRaw(TILE_PLAYER);
    Writeln('   this is you');
    //lmargin:=2;
    //Writeln;
end;

{procedure ManualPage3;
begin
    Writeln;
    Writeln('           SYMBOLS:');
    Writeln;
    //Write(char(TILE_EXIT),char(TILE_EXIT-1)); Writeln(' - exit');
    Position
    PrintRaw ( TILE_EXIT, TILE_EXIT2 ); Writeln(' - exit');
    PrintRaw ( TILE_DARK ); Writeln(' - dark room');
    PrintRaw ( TILE_HOLE ); Writeln(' - collapsed floor');
    PrintRaw ( TILE_DOOR_H, 32, TILE_DOOR_V ); Writeln( ' - closed doors');
    PrintRaw ( TILE_WALL_H, 32, TILE_WALL_V ); Writeln( ' - thin wall');
    Writeln('H - hammer');
    Writeln('L - lantern');
    Writeln('K - key');
    Writeln('P - plank');
    Writeln('B - bandages');
    Writeln('M - medicines');
    Writeln(TILE_PLAYER,' - this is you');
    //lmargin:=2;
    Writeln;
end;
}

function GetArticle(noun : TString) : TString;
begin
    if (noun = 'FOOD') or
       (noun = 'MEDICINE')
    then
	Result := 'SOME'
    else if (noun[1] = 'A') or
       (noun[1] = 'E') or
       (noun[1] = 'I') or
       (noun[1] = 'O')
    then
	Result := 'AN'
    else
	Result := 'A';
end;
