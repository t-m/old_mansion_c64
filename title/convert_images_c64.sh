for IMAGE in $(ls -1 *.gif)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    convert -verbose $IMAGE -colors 2 -negate $NAME.pbm

    # for atari this was enough...
    #tail -c +12 $NAME.pbm > $NAME.gr8
    
    # c64 is more complicated...
    
    tail -c +12 $NAME.pbm > $NAME.raw
    rm $NAME.pbm

    # convert regular raw bitmap to raw c64 screen bitmap
    convert_to_c64/convert_raw_to_c64 ${NAME}.raw ${NAME}.raw_c64

    # and make it easily loadable (directly to screen memory!)
    #echo -n -e '\x00\x20' > $NAME.prg
    echo -n -e '\xC0\x23' > $NAME.prg
    cat ${NAME}.raw_c64 >> $NAME.prg

    # convert to asm include
    convert_to_c64/convert_bin_to_inc ${NAME}.raw_c64 ${NAME}.inc
done

