

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Loading a file from disk
;;; From:
;;;    https://codebase64.org/doku.php?id=base:loading_a_file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	SETNAM = $FFBD
	SETLFS = $FFBA
	LOAD   = $FFD5
ReadFile:
;;;     LDA # fname_end - fname	;
;;;     LDX #<fname
;;;     LDY #>fname
        jsr SETNAM
        lda #$01
        ldx $BA       ; last used device number
        bne ReadFileDefaultDeviceSet
        ldx #$08      ; default to device 8
ReadFileDefaultDeviceSet:
	ldy #$01      ; not $01 means: load to address stored in file
        jsr SETLFS

        lda #$00      ; $00 means  load to memory (not verify)
        jsr LOAD
        bcs ReadFileError    ; if carry set, a load error has happened
        rts
ReadFileError:	
        ; Accumulator contains BASIC error code.
        ; Most likely errors:
        ; A = $05 (DEVICE NOT PRESENT)
        ; A = $04 (FILE NOT FOUND)
        ; A = $1D (LOAD ERROR)
        ; A = $00 (BREAK, RUN/STOP has been pressed during loading)

        ;;  ... error handling ...
        rts	


;;; Wait for keypress
	GETIN = $ffe4
WaitForKey:
	jsr GETIN
	beq WaitForKey
	rts

	
;;; Delay for the time of FOR I=0 TO 100 in BASIC
DelayNtenthS:
	ldx #0
	stx 162
DelayBasicFor100Loop:
	cmp 162		; wait n/60th of a second
	bne DelayBasicFor100Loop
	rts
