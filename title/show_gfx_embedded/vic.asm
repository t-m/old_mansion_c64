;;;
;;; Graphics subroutines for C64 VIC
;;;
;;; Switching graph. modes
;;; Clearing screen, attributes etc.

	
;;; InitScreen
;;; ( https://codebase64.org/doku.php?id=base:built_in_screen_modes )
	VICScrolY  = $d011 	; 53265 Vertical Fine Scrolling
				;       and Control Register (SCROLY)
	VICScrolX = $d016	; 53270 Horizontal Fine Scrolling
				;       and Control Register (SCROLX) 
	VICMemCtrl = $d018	; 53272 VIC-II Chip Memory Control Register
				;       (VMCSB)
;;; $d011 = $3b, $d016 = 8
ScreenBitmapHires:
	;;  	83 POKE 53272, PEEK(53272) OR 8
	lda VICMemCtrl
	ora #8
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32
	lda VICScrolY
	ora #32
	sta VICScrolY
	lda #$8
	sta VICScrolX
	rts

;;; 	$d011 = $3b, $d016 = $18
ScreenBitmapMulticolor:
	;;  	83 POKE 53272, PEEK(53272) OR 8
	lda VICMemCtrl
	ora #8
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32
	;;	lda VICScrolY	ora #32
	lda #$3b
	sta VICScrolY
	lda #$18
	sta VICScrolX
	rts

;;; $d011 = $1b, $d016 = $8
ScreenText:
	lda VICMemCtrl
	and #$8 ^ $FF 		; and not 8 (unset bit 3)
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32

	;; 	lda VICScrolY	ora #32 ^ $FF		; and not 32 (unset bit 5)	sta VICScrolY
	lda #27
	sta VICScrolY
	lda #8
	sta VICScrolX
	rts

;;; $d011 = $1b, $d016 = $8
ScreenTextMulticolor:
	lda VICMemCtrl
	and #$8 ^ $FF 		; and not 8 (unset bit 3)
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32

	;; 	lda VICScrolY	ora #32 ^ $FF		; and not 32 (unset bit 5)	sta VICScrolY
	lda #27
	sta VICScrolY
	lda #18
	sta VICScrolX
	rts


;;; Clear screen attributes
;;; 85 FOR X=1024 TO 2023
;;; 	POKE X,128
;;; 	NEXT
	AttrBase = $400	; 1024
	AttrEnd = 2023
	AttrPtr = $FB
ClearAttr:
	lda #<AttrBase
	sta AttrPtr
	lda #>AttrBase
	sta AttrPtr + 1

	ldx #>AttrEnd
	lda #128		; fill with 128
	ldy #0
ClearAttrPage:
	;; 	sta $2	inc $2	lda $2
	sta (AttrPtr),Y
	iny
	bne ClearAttrPage
	inc AttrPtr + 1
	cpx AttrPtr + 1
	bne ClearAttrPage
ClearAttrPageLast:
	;; 	sta $2	inc $2	lda $2
	sta (AttrPtr),Y
	iny
	cpy #<AttrEnd + 1
	bne ClearAttrPageLast
	rts
	

;;; Clear Graphics Screen
;;; 86 FOR X=8192 TO 16383
;;; 	POKE X,0
;;; 	NEXT
	ScrBase = 8192 		; $2000
	ScrEnd = 16383		; $4000
	ScrPtr = $fb
ClearScr:
	lda #<ScrBase
	sta ScrPtr
	lda #>ScrBase
	sta ScrPtr + 1

	ldx #>ScrEnd
	lda #0			; fill with 0
	ldy #0
ClearScrPage:
	sta (ScrPtr),Y
	iny
	bne ClearScrPage
	inc ScrPtr + 1
	cpx ScrPtr + 1
	bne ClearScrPage
ClearScrPageLast:
	sta (ScrPtr),Y
	iny
	cpy #<ScrEnd + 1
	bne ClearScrPageLast
	rts


;;; Clear Text Screen
	;; 	ScrBase = 8192 		; $2000
	;; 	ScrEnd = 16383		; $4000
	TxtScrBase = $0400		; 1024
	TxtScrEnd = $07e8		; 1024 + 1000
	TxtScrPtr = $fb
ClearTxtScr:
	lda #<TxtScrBase
	sta TxtScrPtr
	lda #>TxtScrBase
	sta TxtScrPtr + 1

	ldx #>TxtScrEnd
	lda #32			; fill with 32 (a whitespace)
	ldy #0
ClearTxtScrPage:
	sta (TxtScrPtr),Y
	iny
	bne ClearTxtScrPage
	inc TxtScrPtr + 1
	cpx TxtScrPtr + 1
	bne ClearTxtScrPage
ClearTxtScrPageLast:
	sta (TxtScrPtr),Y
	iny
	cpy #<TxtScrEnd + 1
	bne ClearTxtScrPageLast
	rts
