
;;;
;;; Args (coords.):
;;;  coords      regs
;;;   X      =    A(H) + X(L)
;;;   Y      =    Y
	CellAddrPtr = $fb
	Screen = $2000
Plot:
	clc
	adc #>Screen
	sta CellAddrPtr + 1
	lda #<Screen
	sta CellAddrPtr
	
	;;  Y
	;;   1 lowest bits -> shift in 8x8 (just add to low offset addr)
	tya
	and #%00000111
	adc CellAddrPtr
	sta CellAddrPtr
	

	tya
	;; and #%11111000	; 3 lowest bit already used (above)
	lsr
	lsr
	lsr
	tay			; the rest adds to offset high
	clc
	adc CellAddrPtr + 1 	; offset += ( Y shr 3 ) * 256
	sta CellAddrPtr + 1

	;;      Y * 64
	tya 			; offset high += Y shr 2
	lsr
	lsr
	clc
	adc CellAddrPtr + 1
	sta CellAddrPtr + 1
	tya			; offset low += Y shl 6 
	asl
	asl
	asl
	asl
	asl
	asl
	clc
	adc CellAddrPtr
	sta CellAddrPtr
	lda #0			; add carry
	adc CellAddrPtr + 1
	sta CellAddrPtr + 1

        ;;  X
	
	;; high 5 bits just adds to the address (use index Y)
	txa
	and #%11111000
	tay

	;; 3 lowest bits of X -> bit number
 	txa
	and #%00000111
	tax
	jsr BitToMask
	ora ($fb),Y
	sta ($fb),Y
	rts


;;; change eg.
;;;     0 -> 0000 0001b
;;;     1 -> 0000 0010b
;;;     ...
;;;     6 -> 0100 0000b
;;;     7 -> 1000 0000b
;;;
;;; Args:
;;;   X contains bit number (0-7)
;;; Result:
;;;   A contains mask with the bit set
BitToMask:
	lda #%10000000
	cpx #0
	beq BitToMaskEnd
BitToMaskNext:
	lsr
	dex
	bne BitToMaskNext
BitToMaskEnd:
	rts
