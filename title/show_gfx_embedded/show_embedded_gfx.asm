;;;
;;; gfx - a program for testing graphics subroutines for Commodore 64
;;;
;;; 
;;; Notes:
;;; - source for assembler XA ( http://www.floodgap.com/retrotech/xa/ ),
;;;   others may need changes

;;; 

;;; loading address for C64's .PRG
.byt $01, $08 		; standard C64 BASIC program start (0x0801 = 2049)

;;; BASIC - 1 line:
;;;  10 SYS 2061

;;; 0801 0B 08 0A 00 9E 32 30 36
;;; 0809 31 00 00 00
;;;	BasicProgram =
	* = $0801
.byt $0B, $08, $0A, $00, $9E, $32, $30, $36
.byt $31, $00, $00, $00

;;; .code

;;; Main  ( $080D = 2061 )
	jsr ScreenBitmapHires
	;; 	jsr ScreenBitmapMulticolor
	jsr ClearAttr
	jsr ClearScr

	;; 	jsr RysujCos

	jsr ShowTitleScreen
	jsr WaitForKey
	jsr ScreenText
	jsr ClearTxtScr
Exit	rts 			; back to BASIC

ShowTitleScreen:
	lda #<TitleScreenEn
	sta SrcBeginL
	lda #>TitleScreenEn
	sta SrcBeginH

	lda #<TitleScreenEnEnd
	sta SrcEndL
	lda #>TitleScreenEnEnd
	sta SrcEndH

	;; set default hires screen as dest.
	lda #$00
	sta DstL
	lda #$20
	sta DstH
	jsr MemCpy
	rts

#include "vic.asm"
#include "util.asm"
#include "relocate.asm"
	
#include "title_en.inc"

