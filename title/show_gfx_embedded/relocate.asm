;;;
;;; MemCpy - a memory relocation (copy) subroutine (like MemCpy)
;;;
;;; Require setting:
;;; - the address of the beginning and the end of the source data
;;; - the address of the beginning of the destination
;;;
;;; (C) 1988-2023 T. Wolak <tomas.wolak@gmail.com>
;;;
;;; uwagi:
;;; - obszary zrodla i celu nie moga na siebie zachodzic(!),
;;;   Sam algorytm zadzialalby gdyby obszar docelowy byl PRZED zrodlem - ale wowczas
;;;   zostalby nadpisany sam program relokujacy - wiec raczej nie zadziala...
;;;   (chyba, ze uklad danych i programu w pamieci bedzie inny...)
;;; - zrodlo dla assemblera XA ( http://www.floodgap.com/retrotech/xa/ ),
;;;   inne moga wymagac poprawek
;;; 


;;; input parameters:
;;; 	the addresses of the beginning and the end of data
;;;     the addresses of the beginning of destination

SrcBegin:
SrcBeginL:
	.byt $00
SrcBeginH:
	.byt $00

SrcEnd:
SrcEndL:
	.byt $01
SrcEndH:
	.byt $00

Dst:
DstL:
	.byt $00
DstH:
	.byt $C0

;;; Zmienne
	;; src and dst on zeropage (for speed)
	SrcLP     = $fb
	SrcHP     = $fc
	DstLP     = $fd
	DstHP     = $fe
	;; koniec danych w obszarze danych uzytkownika
SrcEndLP:
	.word $00, $00		;  = $02A7
SrcEndHP:
	.word $00, $00 		;  = $02A8

;;; 
MemCpy:
	lda SrcBeginL     	; $ML. BAJT ADRESU POCZATKU DANYCH
	sta SrcLP
	lda SrcBeginH		; $ST. BAJT ADRESU POCZATKU DANYCH
	sta SrcHP

	lda DstL		; ML. BAJT ADRESU POCZATKU PROGRAMU
	sta DstLP
	lda DstH		; ST. -"--"--"--"--"--"--"--"--"--"-
	sta DstHP

	lda SrcEndL		; ML. BAJT ADRESU KONCA DANYCH
	sta SrcEndLP
	lda SrcEndH		; ST. BAJT ADRESU KONCA DANYCH
	sta SrcEndHP
	

;;; wlasciwy program przepisujacy dane

	ldy SrcHP		; por. st bajt poczatku i konca zrodla
	cpy SrcEndHP		; jesli taki sam to <= 255 danych do przepisania
	bne MoreThan255

	;; max. 255 bytes to copy
LessThan255:
	ldy #$FF		; $FF so that we have 0 starting (INY in the loop below)
Loop1:
	iny
	lda (SrcLP),Y
	sta (DstLP),Y
	cpy SrcEndLP
	bne Loop1

	;; exit -
	;; either back (eg. to BASIC)
	rts
	;; or jump to program
	;; JMP PrgStart

MoreThan255:
	;; najpiejw przepisz dane dla dopelnienia bajta adresu zrodla (od adr do $FF)
	;; (by go wyzerowac i potem latwo poslugiwac sie rej. indeksowym)
	ldy #$FF
Loop2:
	iny
	lda (SrcLP),Y
	sta (DstLP),Y
	cpy SrcLP
	bne Loop2

	;; zaktualizuj adres docelowy (zwieksz o tyle ile danych przepisalismy)
	lda #$00
	sbc SrcLP
	clc			; check if needed
	adc DstLP
	sta DstLP
	bcc UpdateSrc
	inc DstHP		; zwieksz st. bajt jesli z dodawania w mlodszym bajcie bylo "Carry"
UpdateSrc:
	lda #$00
	sta SrcLP
	inc SrcHP

	;; skopiuj strone ($nn00 do (max) $nnFF)
CopyPage:
	ldx SrcHP		; por. st bajt poczatku i konca zrodla
	cpx SrcEndHP		; jesli taki sam to zostalo <= 255 bajtow do przepisania
	beq LessThan255

	ldy #$00
Loop3:
	lda (SrcLP),Y
	sta (DstLP),Y
	iny
	cpy #$00
	bne Loop3
	inc SrcHP
	inc DstHP
	;; 	JMP CopyPage
	;; faster to use BNE - after INC zero flag never set (cannot overflow dst. address)
	bne CopyPage
