#include <stdio.h>
#include <string.h>

#define BUFSIZE 8

int main ( const int          argc,
           const char * const argv[] )
{
    if ( argc < 3 )
        return 1;

    FILE * const src_file = fopen (argv[1], "rb");
    if ( src_file == NULL ) {
        fprintf ( stderr, "Cannot open source file %s.\n",
                  argv[1] );
        return 1;
    }

    FILE * const dst_file = fopen (argv[2], "wb");
    if ( dst_file == NULL ) {
        fprintf ( stderr, "Cannot open destination file %s.\n",
                  argv[2] );
        fclose ( src_file );
        return 1;
    }

    printf ("Converting a binary data file %s to an assembly include file %s... ",
            argv[1], argv[2] );

    int status = 0;
    unsigned char inbuf[BUFSIZE];
    char outbuf[BUFSIZE*10];
    while ( fread ( inbuf, BUFSIZE, 1, src_file ) == 1 ) {
        sprintf ( outbuf, "\t.byt $%02x, $%02x, $%02x, $%02x, $%02x, $%02x, $%02x, $%02x\n",
                  inbuf[0], inbuf[1], inbuf[2], inbuf[3],
                  inbuf[4], inbuf[5], inbuf[6], inbuf[7] );
        fwrite ( outbuf, strlen ( (char *) outbuf ), 1, dst_file );
    }

    printf ("Done!\n");
    
//clean_up:
    fclose ( src_file );
    fclose ( dst_file );

    return status;
}
