#include <stdio.h>
#include <string.h>

void transform_to_c64(char * const screen);

//void transform_to_c64 ( const char * const src,
//                        char * const dst );
#define SCREEN_RES_X 320
#define SCREEN_RES_Y 200
#define SCREEN_SIZE  SCREEN_RES_X * SCREEN_RES_Y

int main ( const int          argc,
           const char * const argv[] )
{
    if ( argc < 3 ) {
        fprintf ( stderr, "Usage: %s bmp.raw bmp.c64\n", argv[0] );
        return 1;
    }

    FILE * const src_file = fopen (argv[1], "rb");
    if ( src_file == NULL ) {
        fprintf ( stderr, "Cannot open source file %s.\n",
                  argv[1] );
        return 1;
    }

    FILE * const dst_file = fopen (argv[2], "wb");
    if ( dst_file == NULL ) {
        fprintf ( stderr, "Cannot open destination file %s.\n",
                  argv[2] );
        fclose ( src_file );
        return 1;
    }

    printf ("Converting raw bitmap file %s to C64 screen format %s... ",
            argv[1], argv[2] );

    char screen [ SCREEN_SIZE ];  // using 320x146
    //const size_t screen_size = 5840;  // 320 * 146 / 8
    int status = 0;
    size_t bmp_size_read = fread ( screen, 1, SCREEN_SIZE, src_file );
    printf ("input bitmap size %lu... ", bmp_size_read );
    //if ( nread < screen_size ) {
    if ( bmp_size_read % 8 != 0 ) {    // the size of a valid bitmap file for c64 must by divisible by 8
                                  // (8 bytes in each character cell)
        fprintf (stderr, "Error reading file %s.\n", argv[1]);
        status = 1;
        goto clean_up;
    }

    transform_to_c64 ( screen );

    /* for c64, we must (!) have number of horizontal lines of the raw bitmap divisible by 8
       since the graphics is stored in characters 8x8 pixels (40 characters per line)
       so in case the bitmap read above have less lines, we must extend it for c64... */

    size_t bmp_size_to_write = ( bmp_size_read % 320 == 0 ) ?
        bmp_size_read : ( bmp_size_read / 320 + 1 ) * 320;
    printf ("output bitmap size %lu... ", bmp_size_to_write );
    size_t bmp_size_written = fwrite ( screen, 1, bmp_size_to_write, dst_file );
    if ( bmp_size_written != bmp_size_to_write ) {
        fprintf (stderr, "Error writing file %s.\n", argv[2]);
        status = 1;
    } else
        printf ("Done!\n");
    
clean_up:
    fclose ( src_file );
    fclose ( dst_file );

    return status;
}


void transform_to_c64 ( char * const screen )
{
    char dst[SCREEN_SIZE];

//    printf ("\n i   j   k       src               dst\n");
    for ( int k = 0 ; k < 25 ; k++ ) {
        int line_offset = k * 320;
        for ( int j = 0 ; j < 8 ; j++ )
            for ( int i = 0 ; i < 40 ; i++ ) {
                unsigned dst_i = line_offset + j * 40 + i,
                         src_i = line_offset + i * 8 + j;
/*               if ( src_i >= 0x1670 )
                    printf ("%2d  %2d  %2d   %4u / 0x%04x    %4u / 0x%04x\n",
                            i, j, k,  src_i, src_i, dst_i, dst_i );
*/
                dst[ line_offset + i * 8 + j ] = screen[ line_offset + j * 40 + i ];
            }
    }
    
    memcpy ( screen, dst, 320 * 200);
}



/*

static inline void
    swap_char ( char * const c1,
                char * const c2 )
{
    char tmp = *c1;
    *c1 = *c2;
    *c2 = tmp;
}
*/
/*
void transform_to_c64(char * screen)
{
    int i = 40,
        j = 1;
    for ( ; i < 40 * 8 ; i += 40, j++ ) {
        if ( j % 8 == 0 )
            i = ( j / 8 ) * 40 + ( j / 8 );
        swap_char ( &screen[i], &screen[j] );
    }
}
*/
/*
void transform_to_c64 ( const char * const src,
                        char * const dst )
{
    int i = 40,
        j = 1;
    for ( ; j < 320 * 146 / 8 ; j++ ) {
        if ( j % 8 == 0 ) {
            j++;
            i = ( j / 8 ) * 40 + ( j / 8 );
            continue;
        }
        //swap_char ( &screen[i], &screen[j] );
        dst[i] = src[j];
        i += 40;

    }
}
*/
/*
void transform_to_c64 ( char * const screen )
{
    int i = 40,
        j = 1;
    //for ( ; j < 320 * 146 / 8 ;  ) {
    for ( ; j < 40 ;  ) {
        //if ( j % 40 == 0 ) {
            // next block
        //    j += 40 * 7;
        //    continue;
        //}
        if ( j % 8 == 0 ) {
            // next column in block
            //j++;
            i = ( j / 8 ) * 40 + ( j / 8 ) % 5;
            //continue;
        }
        printf (" i %3d <-> j %3d\n", i, j );
        swap_char ( &screen[i], &screen[j] );
        i += 40;
        j++;

    }
}
*/
