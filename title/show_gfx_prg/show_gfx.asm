;;;
;;; gfx - a program for testing graphics subroutines for Commodore 64
;;;
;;; 
;;; Notes:
;;; - source for assembler XA ( http://www.floodgap.com/retrotech/xa/ ),
;;;   others may need changes

;;; 

;;; loading address for C64's .PRG
.byt $01, $08 		; standard C64 BASIC program start (0x0801 = 2049)

;;; BASIC - 1 line:
;;;  10 SYS 2061

;;; 0801 0B 08 0A 00 9E 32 30 36
;;; 0809 31 00 00 00
;;;	BasicProgram =
	* = $0801
.byt $0B, $08, $0A, $00, $9E, $32, $30, $36
.byt $31, $00, $00, $00

;;; .code

;;; Main  ( $080D = 2061 )
	jsr ScreenBitmapHires
	;; 	jsr ScreenBitmapMulticolor
	jsr ClearAttr
	jsr ClearScr

	;; 	jsr RysujCos

	jsr LoadScreen
	jsr WaitForKey
	jsr ScreenText
Exit	rts 			; back to BASIC


;;; InitScreen
;;; ( https://codebase64.org/doku.php?id=base:built_in_screen_modes )
	VICScrolY  = $d011 	; 53265 Vertical Fine Scrolling
				;       and Control Register (SCROLY)
	VICScrolX = $d016	; 53270 Horizontal Fine Scrolling
				;       and Control Register (SCROLX) 
	VICMemCtrl = $d018	; 53272 VIC-II Chip Memory Control Register
				;       (VMCSB)
;;; $d011 = $3b, $d016 = 8
ScreenBitmapHires:
	;;  	83 POKE 53272, PEEK(53272) OR 8
	lda VICMemCtrl
	ora #8
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32
	lda VICScrolY
	ora #32
	sta VICScrolY
	lda #$8
	sta VICScrolX
	rts

;;; 	$d011 = $3b, $d016 = $18
ScreenBitmapMulticolor:
	;;  	83 POKE 53272, PEEK(53272) OR 8
	lda VICMemCtrl
	ora #8
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32
	;;	lda VICScrolY	ora #32
	lda #$3b
	sta VICScrolY
	lda #$18
	sta VICScrolX
	rts

;;; $d011 = $1b, $d016 = $8
ScreenText:
	lda VICMemCtrl
	and #$8 ^ $FF 		; and not 8 (unset bit 3)
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32

	;; 	lda VICScrolY	ora #32 ^ $FF		; and not 32 (unset bit 5)	sta VICScrolY
	lda #27
	sta VICScrolY
	lda #8
	sta VICScrolX
	rts

;;; $d011 = $1b, $d016 = $8
ScreenTextMulticolor:
	lda VICMemCtrl
	and #$8 ^ $FF 		; and not 8 (unset bit 3)
	sta VICMemCtrl
	;; 84 POKE 53265, PEEK(53265) OR 32

	;; 	lda VICScrolY	ora #32 ^ $FF		; and not 32 (unset bit 5)	sta VICScrolY
	lda #27
	sta VICScrolY
	lda #18
	sta VICScrolX
	rts


;;; Clear screen attributes
;;; 85 FOR X=1024 TO 2023
;;; 	POKE X,128
;;; 	NEXT
	AttrBase = $400	; 1024
	AttrEnd = 2023
	AttrPtr = $FB
ClearAttr:
	lda #<AttrBase
	sta AttrPtr
	lda #>AttrBase
	sta AttrPtr + 1

	ldx #>AttrEnd
	lda #128		; fill with 128
	ldy #0
ClearAttrPage:
	;; 	sta $2	inc $2	lda $2
	sta (AttrPtr),Y
	iny
	bne ClearAttrPage
	inc AttrPtr + 1
	cpx AttrPtr + 1
	bne ClearAttrPage
ClearAttrPageLast:
	;; 	sta $2	inc $2	lda $2
	sta (AttrPtr),Y
	iny
	cpy #<AttrEnd + 1
	bne ClearAttrPageLast
	rts
	

;;; Clear Screen
;;; 86 FOR X=8192 TO 16383
;;; 	POKE X,0
;;; 	NEXT
	ScrBase = 8192 		; $2000
	ScrEnd = 16383		; $4000
	ScrPtr = $FB
ClearScr:
	lda #<ScrBase
	sta ScrPtr
	lda #>ScrBase
	sta ScrPtr + 1

	ldx #>ScrEnd
	lda #0			; fill with 0
	ldy #0
ClearScrPage:
	sta (ScrPtr),Y
	iny
	bne ClearScrPage
	inc ScrPtr + 1
	cpx ScrPtr + 1
	bne ClearScrPage
ClearScrPageLast:
	sta (ScrPtr),Y
	iny
	cpy #<ScrEnd + 1
	bne ClearScrPageLast
	rts



ScreenFilename:
	.asc "TITLE_EN.PRG"
ScreenFilenameEnd:

ScreenReadError:
	.byt 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Load Screen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
LoadScreen:
	lda #ScreenFilenameEnd - ScreenFilename
	ldx #<ScreenFilename
	ldy #>ScreenFilename
	jsr ReadFile
        bcs LoadScreenError    ; if carry set, a load error has happened
        rts
LoadScreenError:
	inc ScreenReadError
	rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Loading a file from disk
;;; From:
;;;    https://codebase64.org/doku.php?id=base:loading_a_file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	SETNAM = $FFBD
	SETLFS = $FFBA
	LOAD   = $FFD5
ReadFile:
;;;     LDA # fname_end - fname	;
;;;     LDX #<fname
;;;     LDY #>fname
        jsr SETNAM
        lda #$01
        ldx $BA       ; last used device number
        bne ReadFileDefaultDeviceSet
        ldx #$08      ; default to device 8
ReadFileDefaultDeviceSet:
	ldy #$01      ; not $01 means: load to address stored in file
        jsr SETLFS

        lda #$00      ; $00 means  load to memory (not verify)
        jsr LOAD
        bcs ReadFileError    ; if carry set, a load error has happened
        rts
ReadFileError:	
        ; Accumulator contains BASIC error code.
        ; Most likely errors:
        ; A = $05 (DEVICE NOT PRESENT)
        ; A = $04 (FILE NOT FOUND)
        ; A = $1D (LOAD ERROR)
        ; A = $00 (BREAK, RUN/STOP has been pressed during loading)

        ;;  ... error handling ...
        rts


	

	
;;;
ScreenXL: .byt 0
ScreenXH: .byt 0
ScreenY:  .byt 0

RysujCos:
	lda #0
	sta ScreenXH
	sta ScreenXL
	sta ScreenY
RysujCosNext:
	lda ScreenXH
	ldx ScreenXL
	ldy ScreenY
	jsr Plot
	;;  	lda #1			; delay 1/60 s 	jsr DelayNtenthS
	inc ScreenY
	lda ScreenY
	cmp #200
	bne RysujCosY
	lda #0
	sta ScreenY
RysujCosY:
	inc ScreenXL
	lda ScreenXL
RysujCosCmp:
	cmp #0	
	bne RysujCosNext
	inc ScreenXH
	lda ScreenXH
	cmp #2
	beq RysujCosEnd 
	ldx #$40			; $3F + 1 (319 - 256 = $3F)
	stx RysujCosCmp + 1 	; write to cmp above (!)
	                        ; (we count to 319 = $01 3F )
	clc
	bcc RysujCosNext

RysujCosEnd:
	rts



RysujCos2:
	lda #0
	ldx #0
	ldy #0
	jsr Plot

	lda #0
	ldx #1
	ldy #1
	jsr Plot

	lda #1
	ldx #$3F		; 319
	ldy #7
	jsr Plot

	lda #1
	ldx #$3E
	ldy #7
	jsr Plot

	lda #0
	ldx #0
	ldy #8
	jsr Plot

	lda #1
	ldx #$3F
	ldy #8
	jsr Plot

	
	lda #0
	ldx #0
	ldy #$c7		; 199
	jsr Plot

	lda #1
	ldx #$3F
	ldy #$c7		; 199
	jsr Plot
	
	rts
	
	
;;;
;;; Args (coords.):
;;;  coords      regs
;;;   X      =    A(H) + X(L)
;;;   Y      =    Y
	CellAddrPtr = $fb
	Screen = $2000
Plot:
	clc
	adc #>Screen
	sta CellAddrPtr + 1
	lda #<Screen
	sta CellAddrPtr
	
	;;  Y
	;;   1 lowest bits -> shift in 8x8 (just add to low offset addr)
	tya
	and #%00000111
	adc CellAddrPtr
	sta CellAddrPtr
	

	tya
	;; and #%11111000	; 3 lowest bit already used (above)
	lsr
	lsr
	lsr
	tay			; the rest adds to offset high
	clc
	adc CellAddrPtr + 1 	; offset += ( Y shr 3 ) * 256
	sta CellAddrPtr + 1

	;;      Y * 64
	tya 			; offset high += Y shr 2
	lsr
	lsr
	clc
	adc CellAddrPtr + 1
	sta CellAddrPtr + 1
	tya			; offset low += Y shl 6 
	asl
	asl
	asl
	asl
	asl
	asl
	clc
	adc CellAddrPtr
	sta CellAddrPtr
	lda #0			; add carry
	adc CellAddrPtr + 1
	sta CellAddrPtr + 1

        ;;  X
	
	;; high 5 bits just adds to the address (use index Y)
	txa
	and #%11111000
	tay

	;; 3 lowest bits of X -> bit number
 	txa
	and #%00000111
	tax
	jsr BitToMask
	ora ($fb),Y
	sta ($fb),Y
	rts


;;; change eg.
;;;     0 -> 0000 0001b
;;;     1 -> 0000 0010b
;;;     ...
;;;     6 -> 0100 0000b
;;;     7 -> 1000 0000b
;;;
;;; Args:
;;;   X contains bit number (0-7)
;;; Result:
;;;   A contains mask with the bit set
BitToMask:
	lda #%10000000
	cpx #0
	beq BitToMaskEnd
BitToMaskNext:
	lsr
	dex
	bne BitToMaskNext
BitToMaskEnd:
	rts
	



	

;;; Wait for keypress
	GETIN = $ffe4
WaitForKey:
	jsr GETIN
	beq WaitForKey
	rts

	

	
;;; Delay for the time of FOR I=0 TO 100 in BASIC
DelayNtenthS:
	ldx #0
	stx 162
DelayBasicFor100Loop:
	cmp 162		; wait n/60th of a second
	bne DelayBasicFor100Loop
	rts
