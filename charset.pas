

procedure SetCharset();
begin
    //Poke ( 53272, Peek(53272) and 240 or 12 ); //: REM CHAR SET RAM AT $3000
    Poke ( 53272, ( Peek(53272) and $f0 ) or 8 ); // CHAR SET RAM AT $4000 (8 * $0800)
    //Poke ( 53272, ( Peek(53272) and $0F ) or $14 ); // CHAR SET RAM AT $5000 ($14 * $0400)
end;


procedure CopyCharsetROMToRam();
begin
    Poke ( 56334, Peek(56334) and 254 ); // interrupt off
    Poke ( 1, Peek(1) and 251 );         // charset rom on

    // copy charset
    Move ( //pointer($d000),
           pointer($d800),   // 2nd part of the 4kb (2nd charset, with small letters)
           pointer(CHARSET_VIC),//  pointer($c800),
	  $800 );

    Poke ( 1, Peek(1) or 4 );             // charset rom off
    Poke ( 56334, Peek( 56334) or 1 );    // interrupt on
end;

procedure CopyCustomCharset();
begin
    // copy charset
    //Move ( pointer(CHARSET_BASE),
	//   pointer(CHARSET_VIC),//pointer($c800),
	//   $400 );
    unDEF ( pointer(CHARSET_BASE), pointer(CHARSET_VIC) );   // decompress charset
end;

procedure SetUpVicText();
begin
    CopyCharsetROMToRam();
    CopyCustomCharset();
    
    // setup VIC
    //Poke ( $dd02, Peek($dd02) or 3);  // 56578
    //Poke ( $dd00, Peek($dd00) and 252 );  // 56576
    Poke ($dd00, Peek ($dd00) and $fc); // VIC bank 3, base $c000

    // charset on $c800 (VIC base: $c000 !, bank c000-ffff)
    //Poke ( $d018, Peek ($d018) and 240 or 2); // 53272,
                                                 // and $f0 or 2 -> $800 -> $c800
    Poke ( $d018, 2); // charset $c800, char screen $c000

    // Set kernal charset address
    Poke ( $288, $c0 );  // $c800 shr 8
end;


{

procedure CopyCharsetToRamAsm();assembler;
asm
// COPY $D000-$DFFF -> $3000-$3FFF

    // interrupt off
    lda 56334
    and #254
    sta 56334

    // charset rom on
    lda 1
    and #251
    sta 1

    lda #0
    ldy #$d0
    sta 95
    sty 96

    lda #0
    ldy #$e0
    sta 90
    sty 91

    lda #0
    ldy #$d0
    sta 88
    sty 89

    // copy charset
    jsr $a3bf

    // charset rom off
    lda 1
    ora #4
    sta 1

    // interrupt on
    lda 56334
    ora #1
    sta 56334

    // charset at $3000 (RAM!)
    lda 53272
    and #240
    ora #12
    sta 53272

    //Move ( pointer(
end;
}

	  {
https://www.c64-wiki.com/wiki/Character_set
10 REM COPY ROUTINE
11 FOR I=0 TO 26: READ X: POKE 828+I,X: NEXT I
12 DATA 169,000,160,208,133,095,132,096 : REM LDA #0; LDY #$D0; STA 95, STY 96
13 DATA 169,000,160,224,133,090,132,091 : REM LDA #0; LDY #$E0; STA 90; STY 91
14 DATA 169,000,160,064,133,088,132,089 : REM LDA #0; LDY #$40; STA 88; STY 89
15 DATA 076,191,163 : REM JMP $A3BF
16 REM COPY $D000-$DFFF -> $3000-$3FFF
20 REM CHAR SET ROM INTO RAM 
21 POKE 56334,PEEK(56334) AND 254 : REM INTERRUPT OFF
22 POKE 1,PEEK(1) AND 251 : REM CHAR SET ROM ON
23 SYS 828 : REM START COPY
24 POKE 1,PEEK(1) OR 4 : REM CHAR SET ROM OFF
25 POKE 56334,PEEK(56334) OR 1 : REM INTERRUPT ON
26 POKE 53272,PEEK(53272) AND 240 OR 12 : REM CHAR SET RAM AT $3000
60 REM GERMAN MUTATED VOWEL
61 FOR A=12504 TO 12535: READ ZE: POKE A,ZE: NEXT A
62 DATA 195,024,102,126,102,102,102,000: REM AE-Ä
63 DATA 102,060,102,102,102,102,060,000: REM OE-Ö
64 DATA 102,000,102,102,102,102,060,000: REM UE-Ü
65 DATA 126,102,102,126,102,126,096,096: REM SS-ß
	  
	  }
{
// adresy pocz. i konca danych do przepisania
// oraz miejsca docelowego
const 
    SrcBeginH = $08;  SrcBeginL = $73;
    SrcEndH   = $18;  SrcEndL   = $62; 
    DstH      = $C0;  DstL      = $00;

procedure MemCpy (src_start, src_end, dst_start : word);assembler;
asm
	SrcBeginL = $73
	SrcBeginH = $08
	SrcEndL   = $62
	SrcEndH   = $18
	DstL      = $00
	DstH      = $C0

//;;; zmienne
        // src and dst on zeropage (for speed)
	SrcLP     = $fb
	SrcHP     = $fc
	DstLP     = $fd
	DstHP     = $fe
	//;; koniec danych w obszarze danych uzytkownika
	//SrcEndLP  = $02a7
	//SrcEndHP  = $02a8

//;;; main  //( $080d = dec 2061)

	lda #SrcBeginL     	//; ml. bajt adresu poczatku danych
	sta SrcLP
	lda #SrcBeginH		//; st. bajt adresu poczatku danych
	sta SrcHP

	lda #DstL		//; ml. bajt adresu poczatku programu
	sta DstLP
	lda #DstH		//; st. -"--"--"--"--"--"--"--"--"--"-
	sta DstHP

	lda #SrcEndL		//; ml. bajt adresu konca danych
	sta SrcEndLP
	lda #SrcEndH		//; st. bajt adresu konca danych
	sta SrcEndHP
	

//;;; wlasciwy program przepisujacy dane

	ldy SrcHP		//; por. st bajt poczatku i konca zrodla
	cpy SrcEndHP		//; jesli taki sam to <= 255 danych do przepisania
	bne MoreThan255

	//;; max. 255 bytes to copy
LessThan255:
	ldy #$FF		; $FF so that we have 0 starting (INY in the loop below)
Loop1:
	iny
	lda (SrcLP),y
	sta (DstLP),y
	cpy SrcEndLP
	bne Loop1

	//;; exit -
	//;; either back (eg. to BASIC)
	rts
	//;; or jump to program start
	//;; 	jmp PrgStart

MoreThan255:
	//;; najpiejw przepisz dane dla dopelnienia bajta adresu zrodla (od adr do $FF)
	//;; (by go wyzerowac i potem latwo poslugiwac sie rej. indeksowym)
	ldy #$ff
Loop2:
	iny
	lda (SrcLP),y
	sta (DstLP),y
	cpy SrcLP
	bne Loop2

	//;; zaktualizuj adres docelowy (zwieksz o tyle ile danych przepisalismy)
	lda #$00
	sbc SrcLP
	clc			//; check if needed
	adc DstLP
	sta DstLP
	bcc UpdateSrc
	inc DstHP		//; zwieksz st. bajt jesli z dodawania w mlodszym bajcie bylo "Carry"
UpdateSrc:
	lda #$00
	sta SrcLP
	inc SrcHP

	//;; skopiuj strone ($nn00 do (max) $nnFF)
CopyPage:
	ldx SrcHP		//; por. st bajt poczatku i konca zrodla
	cpx SrcEndHP		//; jesli taki sam to zostalo <= 255 bajtow do przepisania
	beq LessThan255

	ldy #$00
Loop3:
	lda (SrcLP),Y
	sta (DstLP),Y
	iny
	cpy #$00
	bne Loop3
	inc SrcHP
	inc DstHP
	//;; 	JMP CopyPage
	//;; faster to use BEQ - after INC zero flag never set (cannot overflow dst. address)
	bne CopyPage

end;
}
