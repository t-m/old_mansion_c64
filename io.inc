
type String40 = string[40];

var
    //text_screen : array[ 0 .. 40 * 25 - 1] of byte absolute $2000;
    fscr	  : word;
    colorPtr	  : word;


procedure InitIO;
begin
    //Assign(fscr, 'S:'); 
    //Rewrite(fscr, 1);
    fscr     := TXT_SCREEN_BASE;
    colorPtr := TXT_SCREEN_COLOR_BASE;
end;

procedure PrintRawNoColor( c : byte ); overload;
begin
    //blockwrite(fscr, c, 1);
    //Move ( pointer (fscr), c, 1 );
    Write(' ');  // hack to move system cursor position
    Poke ( fscr, c );
    //Poke ( colorPtr, peek(646) ); // use color set by TextColor
    Inc(fscr);
    Inc(colorPtr);
end;

procedure PrintRaw( c : byte ); overload;
var color : byte;
begin
    // place character
    Write(' ');  // hack to move system cursor position

    // convert petscii/ascii low letters to C64 screen codes
    // -> done in new putchar
    //if ( c >= $41 ) and ( c <= $60 ) then c := c - $40;
    Poke ( fscr, c );

    // set color
    color := peek(646);  // the color set by TextColor is the default
    case c of
      TILE_DOOR_H,
      TILE_DOOR_V	: color := LIGHT_GREY;
      TILE_PLAYER	: color := WHITE;
      TILE_BORDER_H,
      TILE_BORDER_V,
      91,                                // inner wall (cross / corners)
      107, 115,                          // left / right vertical border wall
      109, 125,                          // left / right corner of the bottom border wall
      112, 110,                          // left / right corner of the top border wall
      114, 113		: color := GREY; // top / bottom horizontal border wall
      TILE_WALL_H,
      TILE_WALL_V	: color := LIGHT_GREY;
      TILE_ENTRANCE_H,
      TILE_ENTRANCE_V	: color := GREY;
      TILE_EXIT,
      TILE_EXIT2,
      TILE_EXIT2_MANUAL	: color := WHITE;
    end;
    Poke( colorPtr, color );

    // move pointers
    Inc(fscr);
    Inc(colorPtr);
end;

procedure PrintRaw( c1, c2 : byte ); overload;
begin
{    blockwrite(fscr, c1, 1);
    blockwrite(fscr, c2, 1);}
    PrintRaw(c1);
    PrintRaw(c2);
end;

procedure PrintRaw( c1, c2, c3 : byte ); overload;
begin
//{    blockwrite(fscr, c1, 1);
//    blockwrite(fscr, c2, 1);}
    PrintRaw(c1, c2);
    PrintRaw(c3);
end;

procedure PrintRawNoColor(s : string); overload;
var len, i : byte;
begin
    len := length(s);

    // hack to move system cursor
    // (to optimize! with some peek/poke?)
    for i:=1 to len do
	write(' ');  

    {    blockwrite(fscr, s[1], length(s));}
    Move ( s[1], pointer(fscr), len );
    fscr := fscr + len;
    colorPtr := colorPtr + len;
end;


procedure PrintRaw(s : string); overload;
var len, i : byte;
begin
    len := length(s);
    for i:=1 to len do
	PrintRaw( ord(s[i]) );
end;

procedure PrintRaw(s1, s2 : string); overload;
begin
    PrintRaw(s1);
    PrintRaw(s2);
end;

procedure PrintRaw(s1, s2, s3 : string); overload;
begin
    PrintRaw(s1,s2);
    PrintRaw(s3);
end;

procedure PrintRaw(s1, s2, s3, s4 : string); overload;
begin
    PrintRaw(s1,s2);
    PrintRaw(s3,s4);
end;


//procedure Print(s : String40);overload;
//var len	: byte;
//begin
//    len := length(s);
//{    blockwrite(fscr, s[1], length(s));}
//    Move ( s[1], pointer(fscr), len );
//    fscr := fscr + len;
//end;

//procedure Print(c : char); overload;
//begin
//    //blockwrite(fscr, c, 1);
//    //Move ( pointer (fscr), c, 1 );
//    Write(' ');  // hack to move system cursor position
//    Poke ( fscr, ord(c) );
//    Inc(fscr);
//end;

//procedure Print(c1, c2 : char); overload;
//begin
//{    blockwrite(fscr, c1, 1);
//    blockwrite(fscr, c2, 1);}
//    Print(c1);
//    Print(c2);
//end;

function GetOffset(x, y : byte) : word;
begin
    Result := y * 40 + x;
end;

procedure Position(x, y : byte);
var offset : word;
begin
    gotoxy(x + 1, y + 1);  // move system position

    offset   := GetOffset(x, y);
    fscr     := TXT_SCREEN_BASE       + offset;
    colorPtr := TXT_SCREEN_COLOR_BASE + offset;
end;

function Locate(x, y : byte) : byte;
begin
{    result := Antic2Atascii(peek(savmsc + (y * 40) + x));}
    result := peek( TXT_SCREEN_BASE + GetOffset(x, y) );
end;

function GetColor(x, y : byte) : byte;
begin
    result := peek( TXT_SCREEN_COLOR_BASE + GetOffset(x, y) );
end;



{
function GetKey : byte; overload;
begin
    result := byte(ReadKey) and %01011111;
end;

function GetKey(a, b : byte) : byte; overload;
begin
    repeat 
        result := byte(ReadKey) and %01011111;
    until (result = a) or (result = b);
end;

function GetKey(a, b, c, d : byte) : byte; overload;
begin
    repeat 
        result := byte(ReadKey) and %01011111;
    until (result = a) or (result = b) or
          (result = c) or (result = d);
end;
}


function readkey_tw : char; assembler;
asm
    jsr $F142
    sta Result
end;

procedure kbdclearbuf; assembler;
asm
    lda #$00
    sta $c6
    jsr $ffe4
end;



// https://www.c64-wiki.com/wiki/Keyboard_code
function GetKey : char; overload;
begin
    result := char ( Peek ( $EB81 + ord(ReadKey) )  );
//    result := char ( Peek ( $EB81 + ord(ReadKey_tw) )  );
    kbdclearbuf;
end;

function GetKey(a, b : char) : char; overload;
begin
    repeat 
        //result := ReadKey;
	result := GetKey;
    until (result = a) or (result = b);
end;

function GetKey(a, b, c, d : char) : char; overload;
begin
    repeat 
        //result := ReadKey;
	result := GetKey;
    until (result = a) or (result = b) or
          (result = c) or (result = d);
end;



procedure ClearStatusLine;
begin
{    fillbyte(pointer(savmsc+22*40),80,0);}
    fillbyte ( pointer(TXT_SCREEN_BASE + 23 * 40), 80, 32 );
end;

procedure StatusLinePos;
begin
    //Position(2, 22);
    Position(0, 23);
end;

procedure StatusLinePos2;
begin
    //Position(2, 23);
    Position(0, 24);
end;


procedure StatusLine(s: String40);
begin
    TextColor(COLOR_ACTION);
    ClearStatusLine;
    StatusLinePos;
    Write(s);
end;

procedure StatusLineln(s: String40);
begin
    StatusLine(s);
    Writeln;
end;

procedure StatusLine2(s: String40);
begin
    TextColor(COLOR_ACTION);
    StatusLinePos2;
    Write(s);
end;
